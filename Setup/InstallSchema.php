<?php

namespace Pimentos\DPD\Setup;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * @codeCoverageIgnore
 */
class InstallSchema implements InstallSchemaInterface
{

    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();
        $installer->getConnection()->addColumn(
            $installer->getTable('quote'),
            'pickup_point',
            [
                'type' => 'text',
                'nullable' => false,
                'comment' => 'Terminal code',
            ]
        );
        $installer->getConnection()->addColumn(
            $installer->getTable('sales_order'),
            'dpd_last_status_update',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                'nullable' => false,
                'comment' => 'Last status update unixtimestamp',
            ]
        );

        $this->installTable($installer, 'b_ipol_dpd_location');
        $this->installTable($installer, 'b_ipol_dpd_terminal');
        $this->installTable($installer, 'b_ipol_dpd_order');

        $setup->endSetup();
    }

    protected function installTable(SchemaSetupInterface $installer, $tableName)
    {
        $tableName = $installer->getTable($tableName);

        if ($installer->getConnection()->isTableExists($tableName)) {
            return true;
        }

        $sqlPath = __DIR__ .'/../../../../vendor/ipol/dpd.sdk/data/db/install/mysql/'. $tableName .'.sql';
        $sql = str_replace('b_ipol_dpd_order', trim($tableName, '`'), file_get_contents($sqlPath));
        $parts = array_filter(explode(';', $sql));

        foreach ($parts as $sql) {
            $installer->run($sql . ';');
        }

        return true;
    }
}