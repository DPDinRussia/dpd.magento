<?php

namespace Pimentos\DPD\Builder;

class Shipment {

    protected $shipment;
    protected $subtotal;
    protected $taxCalculation;
    
    public function __construct(
        \Ipol\DPD\Config\Config $config,
        $senderCityOption,
        $recipientCountryCode,
        $recipientState,
        $recipientCity,
        $subtotal,
        $allItems,
        \Magento\Tax\Api\TaxCalculationInterface $taxCalculation
    ) {
        $this->shipment = new \Ipol\DPD\Shipment($config);
        $this->setSender($senderCityOption);
        $this->setReceiver($recipientCountryCode, $recipientState, $recipientCity);
        $this->subtotal = $subtotal;
        $this->taxCalculation = $taxCalculation;
        $this->setItems($allItems);
    }

    /**
     * Задает отправителя
     * @param string
     * @return void
     */
    protected function setSender($senderCityOption)
    {
        $senderCity = explode(
            ',', 
            $senderCityOption
        );
        $this->shipment->setSender(
            trim($senderCity[0]),
            trim($senderCity[1]),
            trim($senderCity[2])
        );
    }

    /**
     * Задает получателя
     * @param string
     * @return void
     */
    protected function setReceiver($recipientCountryCode, $recipientState, $recipientCity)
    {
        $countryArr = [
            'RU' => 'Россия',
            'BY' => 'Беларусь',
            'KZ' => 'Казахстан'
        ];

        $this->shipment->setReceiver(
            isset($countryArr[$recipientCountryCode]) ? 
                $countryArr[$recipientCountryCode] : '',
            $recipientState,
            $recipientCity
        );
    }

    /**
     * Задает позиции заказа
     * @param array $allItems Magento\Sales\Model\Order\Item\Interceptor
     */
    protected function setItems(array $allItems)
    {
        $cartItems = [];
        foreach($allItems as $item) {
            if ($item->getHasChildren() && is_array($item->getChildren())) {
                foreach ($item->getChildren() as $child) {
                    $cartItems[] = $child;
                }
            } else {
                $cartItems[] = $item;
            }
        }
        $dpdItems = [];
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $productObject = $objectManager->create('Magento\Catalog\Model\Product');
        foreach ($cartItems as $item) {
            $product = $productObject->load($item->getProductId());
            $price = $item->getPrice();
            if (!intval($price)) {
                $parent = $productObject->load(
                    $product->getTypeInstance()->getParentIdsByChild($item->getProductId())
                );
                if ($parent) {
                    $price = $parent->getPrice();
                }
            }
            $dpdItems[] = [
                'NAME' => $item->getName(),
                'QUANTITY' => $item->getQty(),
                'PRICE' => $price,
                'VAT_RATE' => $this->taxCalculation
                    ->getCalculatedRate($product->getData('tax_class_id')),
                'WEIGHT' => $product->getData('weight') ? : 0,
                'DIMENSIONS' => [
                    'LENGTH' => $product->getTsDimensionsLength() ? : 0,
                    'WIDTH' => $product->getTsDimensionsWidth() ? : 0,
                    'HEIGHT' => $product->getTsDimensionsHeight() ? : 0
                ]
            ];
        }
        $this->shipment->setItems($dpdItems, $this->subtotal);
    }


    public function setDeliveryVariant(string $code)
    {
        switch ($code) {
            case 'ДД':
                $this->shipment->setSelfDelivery(false);
                $this->shipment->setSelfPickup(false);  
            break;
            case 'ДТ':
                $this->shipment->setSelfPickup(false);  
                $this->shipment->setSelfDelivery(true);
            break;
            case 'ТТ':
                $this->shipment->setSelfDelivery(true);
                $this->shipment->setSelfPickup(true);  
            break;
            case 'ТД':
                $this->shipment->setSelfDelivery(false);
                $this->shipment->setSelfPickup(true);  
            break;
        }
    } 

    public function getInstance()
    {
        return $this->shipment;
    }
}