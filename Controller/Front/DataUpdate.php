<?php

namespace Pimentos\DPD\Controller\Front;

class DataUpdate extends \Magento\Framework\App\Action\Action {

    /**
     * @var \Ipol\DPD\Config\Config
     */
    protected $config;

    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $session;

    /**
     * @var \Magento\Tax\Api\TaxCalculationInterface
     */
    protected $helperData;

    /**
     * @var \Magento\Framework\App\ObjectManager
     */
    protected $objectManager;

    /**
     * @param Action\Context $context
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     * @param \Pimentos\DPD\Helper\Data $helperData
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Pimentos\DPD\Helper\Data $helperData,
        \Magento\Checkout\Model\Session $session
    ) {
        $this->helperData = $helperData;
        $this->config = $helperData->generateDpdSdkConfig();
        $this->session = $session;
        $this->objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        parent::__construct($context);
    }

    public function execute()
    {
        \Ipol\DPD\Agents::loadExternalData($this->config);
    }

}