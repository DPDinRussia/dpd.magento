<?php

namespace Pimentos\DPD\Controller\Front;

use Pimentos\DPD\Helper\PriceRules;

class Pickups extends \Magento\Framework\App\Action\Action {

    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $resultJsonFactory;

    /**
     * @var \Ipol\DPD\Config\Config
     */
    protected $config;

    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $session;

    /**
     * @var \Magento\Tax\Api\TaxCalculationInterface
     */
    protected $helperData;

    /**
     * @var \Magento\Framework\App\ObjectManager
     */
    protected $objectManager;

    /**
     * @param Action\Context $context
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     * @param \Pimentos\DPD\Helper\Data $helperData
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Pimentos\DPD\Helper\Data $helperData,
        \Magento\Checkout\Model\Session $session
    ) {
        $this->resultJsonFactory = $resultJsonFactory;
        $this->helperData = $helperData;
        $this->config = $helperData->generateDpdSdkConfig();
        $this->session = $session;
        $this->objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        parent::__construct($context);
    }

    public function execute()
    {
        $countryCode = $this->getRequest()->getPost('country_code') ?
            $this->getRequest()->getPost('country_code') : '';
        $region = $this->getRequest()->getPost('region');
        $city = $this->getRequest()->getPost('city');
        $paymentMethod = $_COOKIE['dpd_payment_method'] ?? '';

        $taxCalculation = $this->objectManager->create('\Magento\Tax\Api\TaxCalculationInterface');

        //создаем конвертер
        $converter = new \Pimentos\DPD\Helper\Converter();
        
        //создаем отправку
        $shipmentFactory = new \Pimentos\DPD\Builder\Shipment(
            $this->config,
            $this->helperData->getData(
                'sender/sender_group/sender_city'
            ),
            $countryCode,
            $region,
            $city,
            $this->session->getQuote()->getOrderSubtotal(),
            $this->session->getQuote()->getAllItems(),
            $taxCalculation
        );
        $shipment = $shipmentFactory->getInstance();
        
        if ($paymentMethod) {
            $shipment->setPaymentMethod(1, $paymentMethod);
        }

        $storeManager = $this->objectManager->create('\Magento\Store\Model\StoreManagerInterface');

        //cчитаем
        $tariffs = [
            'courier' => PriceRules::round($shipment->setSelfDelivery(false)
                ->calculator()->setCurrencyConverter($converter)
                ->calculate($storeManager->getStore()->getBaseCurrencyCode()),
                $this->helperData->getData(
                    'calculation/calculation_group/round_to'
                )
            ),
            'pickup' => PriceRules::round($shipment->setSelfDelivery(true)
                ->calculator()->setCurrencyConverter($converter)
                ->calculate($storeManager->getStore()->getBaseCurrencyCode()),
                $this->helperData->getData(
                    'calculation/calculation_group/round_to'
                )
            ),
        ];

        //получаем терминалы
        $terminals = [];
        $where = 'LOCATION_ID = :location';
        $bind = [
            ':location' => $shipment->getReceiver()['CITY_ID']
        ];
        $ogd = $this->helperData->getData(
            'shipping_description/shipping_description_common_group/ogd'
        );
        if ($ogd) {
            $where .= ' AND SERVICES LIKE :ogd';
            $bind['ogd'] = '%'.$ogd.'%';
        }

        if ($paymentMethod) {
            $where .= ' AND NPP_AVAILABLE =:npp';
            $bind['npp'] = 'Y';
        }

        $terminals = \Ipol\DPD\DB\Connection::getInstance($this->config)
            ->getTable('terminal')->findModels([
                'where' => $where,
                'bind'  => $bind,
            ]);
        $terminals = \Pimentos\DPD\Model\SDK\Terminal::onlyAvaliable(
            $terminals,
            $shipment
        );

        $terminals = array_filter($terminals, function($terminal) use ($shipment) {
            return $terminal->checkShipment($shipment);
        });

        $increaseDeliveryTime = $this->helperData->getData(
            'calculation/calculation_group/add_delivery_day'
        );
        if ($increaseDeliveryTime) {
            foreach ($tariffs as &$tariff) {
                $tariff['DAYS'] += $increaseDeliveryTime;
            }
        }

        return $this->resultJsonFactory
            ->create()
            ->setData([
                'tariffs' => $tariffs,
                'terminals' => array_values($terminals)
        ]);
    }
}
