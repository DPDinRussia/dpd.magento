<?php

namespace Pimentos\DPD\Controller\Front;

class StatusUpdate extends \Magento\Framework\App\Action\Action {

    /**
     * @var \Ipol\DPD\Config\Config
     */
    protected $config;

    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $session;

    /**
     * @var \Magento\Tax\Api\TaxCalculationInterface
     */
    protected $helperData;

    /**
     * @var \Magento\Framework\App\ObjectManager
     */
    protected $objectManager;

    /**
     * @param Action\Context $context
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     * @param \Pimentos\DPD\Helper\Data $helperData
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Pimentos\DPD\Helper\Data $helperData,
        \Magento\Checkout\Model\Session $session
    ) {
        $this->helperData = $helperData;
        $this->config = $helperData->generateDpdSdkConfig();
        $this->session = $session;
        $this->objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        parent::__construct($context);
    }

    public function execute()
    {
        if ($this->helperData->getData(
            'status/status_group/status_order_check'
        )) {
            \Ipol\DPD\Agents::checkOrderStatus($this->config);
            
            $orderTabel = \Ipol\DPD\DB\Connection::getInstance($this->config)->getTable('order');
            $OrderFactory = $this->objectManager
                ->create('Magento\Sales\Model\ResourceModel\Order\CollectionFactory');
            $orderCollection = $OrderFactory->create()->addFieldToSelect(array('*'));
            $orderCollection->addFieldToFilter('dpd_last_status_update', ['lt' => time()]);
            $orderCollection->addFieldToFilter(
                ['shipping_method', 'shipping_method'],
                [
                    ['eq' => 'dpd_pickup'],
                    ['eq' => 'dpd_courier']
                ]
            );
            $orderCollection->getSelect()->limit(20);
            foreach ($orderCollection as $order) {
                $dpdOrder = $orderTabel->getByOrderId($order->getIncrementId());
                if ($dpdOrder) {
                    $magentoStatus = $this->helperData->getData(
                        'status/status_group/sync_order_status_'.$dpdOrder->orderStatus
                    );
                    if ($magentoStatus) {
                        $order->setState($magentoStatus);
                        $order->save();
                    }
                }
            }
        }
    }

}