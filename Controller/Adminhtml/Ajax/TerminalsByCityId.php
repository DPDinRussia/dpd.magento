<?php

namespace Pimentos\DPD\Controller\Adminhtml\Ajax;

use Magento\Backend\App\Action;

class TerminalsByCityId extends Action {

    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $resultJsonFactory;

    protected $config;

    /**
     * @param Action\Context $context
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     * @param \Pimentos\DPD\Helper\Data $helperData
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Pimentos\DPD\Helper\Data $helperData
    ) {
        parent::__construct($context);
        $this->resultJsonFactory = $resultJsonFactory;
        $this->config = $helperData->generateDpdSdkConfig();
    }

    public function execute()
    {
        $cityId = $this->getRequest()->getPost('city_id');
        $db = \Ipol\DPD\DB\Connection::getInstance($this->config);
        $terminalTable = $db->getTable('terminal');
        $items = $terminalTable->find([
            'where' => 'LOCATION_ID = :location_id',
            'bind' => [':location_id' => $cityId]
        ]);
        $result = [
            'html' => ''
        ];
        while($item = $items->fetch()) {
            $result['html'] .= '<option value="'.$item['CODE'].'">'.
                $item['ADDRESS_SHORT'].'</option>';
        }
        return $this->resultJsonFactory->create()->setData($result);
    }
}

// class TerminalsByCityId extends \Magento\Backend\App\Action {

//     /**
//      * @var \Magento\Framework\Controller\Result\JsonFactory
//      */
//     protected $resultJsonFactory;

//     /**
//      * @param Action\Context $context
//      * @param \Magento\Framework\Translate\Inline\ParserInterface $inlineParser
//      * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
//      */
//     public function __construct(
//         \Magento\Backend\App\Action\Context $context,
//         \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
//     ) {
//         parent::__construct($context);
//         $this->resultJsonFactory = $resultJsonFactory;
//     }

//     public function execute()
//     {
//         $cityId = $this->getRequest()->getPost('city_id');
//         $config = new \Ipol\DPD\Config\Config([]);
//         $db = \Ipol\DPD\DB\Connection::getInstance($config);
//         $terminalTable = $db->getTable('terminal');
//         $items = $terminalTable->find([
//             'where' => 'LOCATION_ID = :location_id',
//             'bind' => [':location_id' => $cityId]
//         ]);
//         $result = [
//             'html' => ''
//         ];
//         while($item = $items->fetch()) {
//             $result['html'] .= '<option value="'.$item['CODE'].'">'.
//                 $item['ADDRESS_SHORT'].'</option>';
//         }
//         return $this->resultJsonFactory->create()->setData($result);
//     }
// }