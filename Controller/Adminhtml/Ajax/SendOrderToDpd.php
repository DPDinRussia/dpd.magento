<?php

namespace Pimentos\DPD\Controller\Adminhtml\Ajax;

use Magento\Framework\App\Action\HttpGetActionInterface as HttpGetActionInterface;

use Pimentos\DPD\Helper\SimpleValidation;

class SendOrderToDpd extends \Magento\Backend\App\Action {

    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $resultJsonFactory;

    /**
     * @var \Pimentos\DPD\Helper\Data
     */
    protected $helperData;

    /**
     * @var \Pimentos\DPD\Helper\Data
     */
    protected $config;

    /**
     * @var \Magento\Tax\Api\TaxCalculationInterface
     */
    protected $taxCalculation;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @param Action\Context $context
     * @param \Magento\Framework\Translate\Inline\ParserInterface $inlineParser
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     * @param \Pimentos\DPD\Helper\Data $helperData
     * @param \Magento\Tax\Api\TaxCalculationInterface $taxCalculation
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Pimentos\DPD\Helper\Data $helperData,
        \Magento\Tax\Api\TaxCalculationInterface $taxCalculation,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    ) {
        parent::__construct($context);
        $this->resultJsonFactory = $resultJsonFactory;
        $this->helperData = $helperData;
        $this->config = $helperData->generateDpdSdkConfig();
        $this->taxCalculation = $taxCalculation;
        $this->storeManager = $storeManager;
    }

    public function execute()
    {
        $result = [];
        $errors = [];
        $orderDataForDpd = $this->getRequest()->getPost('order');
        if ($orderDataForDpd){

            //валидация
            $dataForValidation = [
                [
                    'type' => 'date|required',
                    'value' => $orderDataForDpd['pickupDate'],
                    'error' => '<strong>Дата передачи груза в DPD</strong> обязательное для заполнения'
                ],
                [
                    'type' => 'number|required',
                    'value' => $orderDataForDpd['cargoWeight'],
                    'error' => '<strong>Вес отправки, кг</strong> обязательное для заполнения и должно быть числом'
                ],
                [
                    'type' => 'number|required',
                    'value' => $orderDataForDpd['dimensionWidth'],
                    'error' => '<strong>Ширина</strong> обязательное для заполнения и должно быть числом'
                ],
                [
                    'type' => 'number|required',
                    'value' => $orderDataForDpd['dimensionHeight'],
                    'error' => '<strong>Высота</strong> обязательное для заполнения и должно быть числом'
                ],
                [
                    'type' => 'number|required',
                    'value' => $orderDataForDpd['dimensionLength'],
                    'error' => '<strong>Длина</strong> обязательное для заполнения и должно быть числом'
                ],
                [
                    'type' => 'number|required',
                    'value' => $orderDataForDpd['cargoNumPack'],
                    'error' =>  '<strong>Количество грузомест(посылок)</strong> обязательное для заполнения и должно быть числом больше нуля'
                ],
                [
                    'type' => 'required',
                    'value' => $orderDataForDpd['cargoCategory'],
                    'error' => '<strong>Содержимое отправки</strong> обязательное для заполнения'
                ],
                [
                    'type' => 'required',
                    'value' => $orderDataForDpd['sender']['fio'],
                    'error' => 'Отправитель <strong>Контактное лицо</strong> обязательное для заполнения'
                ],
                [
                    'type' => 'required',
                    'value' => $orderDataForDpd['sender']['name'],
                    'error' => 'Отправитель <strong>Имя отправителя</strong> обязательное для заполнения'
                ],
                [
                    'type' => 'required',
                    'value' => $orderDataForDpd['sender']['phone'],
                    'error' => 'Отправитель <strong>Телефон</strong> обязательное для заполнения'
                ],
                // [
                //     'type' => 'email',
                //     'value' => $orderDataForDpd['sender']['email'],
                //     'error' => 'Отправитель <strong>Email отправителя</strong> обязательное для заполнения'
                // ],
                [
                    'type' => 'required',
                    'value' => $orderDataForDpd['recipient']['fio'],
                    'error' => 'Получатель <strong>Контактное лицо</strong> обязательное для заполнения'
                ],
                [
                    'type' => 'required',
                    'value' => $orderDataForDpd['recipient']['name'],
                    'error' => 'Получатель <strong>Имя получателя</strong> обязательное для заполнения'
                ],
                [
                    'type' => 'required',
                    'value' => $orderDataForDpd['recipient']['phone'],
                    'error' => 'Получатель <strong>Телефон</strong> обязательное для заполнения'
                ],
                // [
                //     'type' => 'email',
                //     'value' => $orderDataForDpd['recipient']['email'],
                //     'error' => 'Получатель <strong>Почтовый ящик, email</strong> обязательное для заполнения'
                // ]
            ];

            if ($orderDataForDpd['deliveryVariant'] == 'ДД' ||
                $orderDataForDpd['deliveryVariant'] == 'ДТ'
            ) {
                $dataForValidation[] = [
                    'type' => 'required',
                    'value' => $orderDataForDpd['sender']['street'],
                    'error' => 'Отправитель <strong>Улица</strong> обязательное для заполнения'
                ];
            }
            if ($orderDataForDpd['deliveryVariant'] == 'ДД' ||
                $orderDataForDpd['deliveryVariant'] == 'ТД'
            ) {
                $dataForValidation[] = [
                    'type' => 'required',
                    'value' => $orderDataForDpd['recipient']['street'],
                    'error' => 'Получатель <strong>Улица</strong> обязательное для заполнения'
                ];
            }
            if ($orderDataForDpd['deliveryVariant'] == 'ТТ' ||
                $orderDataForDpd['deliveryVariant'] == 'ТД'
            ) {
                $dataForValidation[] = [
                    'type' => 'required',
                    'value' => $orderDataForDpd['sender']['terminal_code'],
                    'error' => 'Отправитель <strong>Терминал</strong> обязательный для заполнения'
                ];
            }
            if ($orderDataForDpd['deliveryVariant'] == 'ТТ' ||
                $orderDataForDpd['deliveryVariant'] == 'ДТ'
            ) {
                $dataForValidation[] = [
                    'type' => 'required',
                    'value' => $orderDataForDpd['recipient']['terminal_code'],
                    'error' => 'Получатель <strong>Терминал</strong> обязательный для заполнения'
                ];
            }
            $errors = SimpleValidation::validate($dataForValidation);
            
            if (!$errors) {
                $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
                $order = $objectManager->create('Magento\Sales\Model\Order')->load(
                    $orderDataForDpd['orderId']
                );

                //адрес доставки
                $shippingAddress = $order->getShippingAddress();

                //адрес оплаты
                $billingAddress = $order->getBillingAddress();

                //способ оплаты
                $payment = $order->getPayment();
                $paymentMethod = $payment ? $payment->getMethodInstance() : null;

                //конвертер
                $converter = new \Pimentos\DPD\Helper\Converter();

                //создаем отправку
                $shipmentFactory = new \Pimentos\DPD\Builder\Shipment(
                    $this->config,
                    $this->helperData->getData(
                        'sender/sender_group/sender_city'
                    ),
                    $shippingAddress->getCountryId(),
                    $shippingAddress->getRegionCode(),
                    $shippingAddress->getCity(),
                    $order->getSubtotal(),
                    $order->getAllVisibleItems(),
                    $this->taxCalculation
                );
                $shipmentFactory->setDeliveryVariant($orderDataForDpd['deliveryVariant']);
                $shipment = $shipmentFactory->getInstance();

                //задаем габариты
                $shipment->setDimensions(
                   $orderDataForDpd['dimensionWidth'],
                   $orderDataForDpd['dimensionHeight'],
                   $orderDataForDpd['dimensionLength'],
                   $orderDataForDpd['cargoWeight']
                );

                $shipment->setPaymentMethod(1, $paymentMethod->getCode());


                $dpdOrder = \Ipol\DPD\DB\Connection::getInstance($this->config)
                    ->getTable('order')->getByOrderId($order->getIncrementId(), true);

                $dpdOrder->orderId  = $order->getIncrementId();
                $dpdOrder->currency = $this->storeManager->getStore()->getBaseCurrencyCode();

                $dpdOrder->serviceCode = $orderDataForDpd['deliveryType'];
                $dpdOrder->cargoNumPack = $orderDataForDpd['cargoNumPack'];
                $dpdOrder->cargoCategory = $orderDataForDpd['cargoCategory'];

                $dpdOrder->dimensionWidth = $orderDataForDpd['dimensionWidth'];
                $dpdOrder->dimensionHeight = $orderDataForDpd['dimensionHeight'];
                $dpdOrder->dimensionLength = $orderDataForDpd['dimensionLength'];
                $dpdOrder->cargoWeight = $orderDataForDpd['cargoWeight'];

                $dpdOrder->senderName = $orderDataForDpd['sender']['fio'];
                $dpdOrder->senderFio = $orderDataForDpd['sender']['name'];
                $dpdOrder->senderEmail = $orderDataForDpd['sender']['email'];
                $dpdOrder->senderPhone = $orderDataForDpd['sender']['phone'];
                $dpdOrder->senderLocation = $orderDataForDpd['sender']['location'];
                $dpdOrder->senderNeedPass = isset($orderDataForDpd['sender']['need_pass']) ? 'Y' : 'N';
                if ($orderDataForDpd['deliveryVariant'] == 'ТД' ||
                    $orderDataForDpd['deliveryVariant'] == 'ТТ'
                ) {
                    $dpdOrder->senderTerminalCode = $orderDataForDpd['sender']['terminal_code'];
                } else {
                    $dpdOrder->senderStreet = $orderDataForDpd['sender']['street'];
                    $dpdOrder->senderStreetabbr = $orderDataForDpd['sender']['streetabbr'];
                    $dpdOrder->senderKorpus = $orderDataForDpd['sender']['korpus'];
                    $dpdOrder->senderHouse = $orderDataForDpd['sender']['house'];
                    $dpdOrder->senderStr = $orderDataForDpd['sender']['str'];
                    $dpdOrder->senderVlad = $orderDataForDpd['sender']['vlad'];
                    $dpdOrder->senderOffice = $orderDataForDpd['sender']['office'];
                    $dpdOrder->senderFlat = $orderDataForDpd['sender']['flat'];
                }

                $dpdOrder->receiverName = $orderDataForDpd['recipient']['fio'];
                $dpdOrder->receiverFio = $orderDataForDpd['recipient']['name'];
                $dpdOrder->receiverPhone = $orderDataForDpd['recipient']['phone'];
                $dpdOrder->receiverEmail = $orderDataForDpd['recipient']['email'];
                $dpdOrder->receiverNeedPass =
                    isset($orderDataForDpd['recipient']['need_pass']) ?'Y' : 'N';
                $dpdOrder->pickupDate = date('Y-m-d', strtotime($orderDataForDpd['pickupDate']));
                $dpdOrder->receiverLocation = $orderDataForDpd['recipient']['location'];
                if ($orderDataForDpd['deliveryVariant'] == 'ДТ' ||
                    $orderDataForDpd['deliveryVariant'] == 'ТТ'
                ) {
                    $dpdOrder->receiverTerminalCode = $orderDataForDpd['recipient']['terminal_code'];
                    $dpdOrder->pickupTimePeriod = $orderDataForDpd['pickupTimePeriod'];
                } else {
                    $dpdOrder->receiverStreet = $orderDataForDpd['recipient']['street'];
                    $dpdOrder->receiverStreetabbr = $orderDataForDpd['recipient']['streetabbr'];
                    $dpdOrder->receiverKorpus = $orderDataForDpd['recipient']['korpus'];
                    $dpdOrder->receiverHouse = $orderDataForDpd['sender']['house'];
                    $dpdOrder->receiverStr = $orderDataForDpd['recipient']['str'];
                    $dpdOrder->receiverVlad = $orderDataForDpd['recipient']['vlad'];
                    $dpdOrder->receiverOffice = $orderDataForDpd['recipient']['office'];
                    $dpdOrder->receiverFlat = $orderDataForDpd['recipient']['flat'];
                    $dpdOrder->deliveryTimePeriod = $orderDataForDpd['deliveryTimePeriod'];
                }
                $dpdOrder->useCargoValue   = isset($orderDataForDpd['useCargoValue']) ? 'Y' : 'N';
                $dpdOrder->npp             = isset($orderDataForDpd['npp']) ? 'Y' : 'N';
                $dpdOrder->cargoRegistered = isset($orderDataForDpd['cargo_registered']) ? 'Y' : 'N';
                $dpdOrder->dvd             = isset($orderDataForDpd['dvd']) ? 'Y' : 'N';
                $dpdOrder->trm             = isset($orderDataForDpd['trm']) ? 'Y' : 'N';
                $dpdOrder->prd             = isset($orderDataForDpd['prd']) ? 'Y' : 'N';
                $dpdOrder->vdo             = isset($orderDataForDpd['vdo']) ? 'Y' : 'N';
                $dpdOrder->vdo             = isset($orderDataForDpd['vdo']) ? 'Y' : 'N';
                $dpdOrder->ogd             = $orderDataForDpd['ogd'] ? $orderDataForDpd['ogd'] : '';
                $dpdOrder->esz             = $orderDataForDpd['esz'];
                $dpdOrder->paymentType     = $orderDataForDpd['paymentType'];

                try {
                    $dpdOrder->setShipment($shipment);
                    $dpdOrder->setUnitLoads($orderDataForDpd['unit_loads']);

                    $response = $dpdOrder->dpd()
                        ->setCurrencyConverter($converter)
                        ->create();
                }  catch (\SoapFault $e) {
                    $errors[] = $e->getMessage();
                } catch (\Exception $e) {
                    $errors[] = $e->getMessage();
                }

                if (isset($response) && !$response->isSuccess()) {
                    foreach ($response->getErrors() as $error) {
                        $errors[] = $error->getMessage();
                    }
                } else {
                    $statusList = \Ipol\DPD\DB\Order\Model::StatusList();
                    $status = isset($statusList[$dpdOrder->orderStatus]) ?
                        $statusList[$dpdOrder->orderStatus] : 'unknow';
                    $result['success'] = 'Заказ отправлен';
                    $result['data'] = [
                        'id' => $dpdOrder->orderNum,
                        'status' => $status
                    ];
                }
            }
        } else {
            $errors[] = 'Не найден ключевой параметр order';
        }
        if ($errors) {
            $result['error'] = SimpleValidation::errorsHtml($errors);
        }
        return $this->resultJsonFactory
            ->create()
            ->setData($result);
    }
}