<?php

namespace Pimentos\DPD\Controller\Adminhtml\Ajax;

use Magento\Framework\App\Action\HttpGetActionInterface as HttpGetActionInterface;

use Pimentos\DPD\Helper\SimpleValidation;

class PrintDocs extends \Magento\Backend\App\Action {

    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $resultJsonFactory;

    /**
     * @var \Pimentos\DPD\Helper\Data
     */
    protected $helperData;

    /**
     * @var \Pimentos\DPD\Helper\Data
     */
    protected $config;

    /**
     * @param Action\Context $context
     * @param \Magento\Framework\Translate\Inline\ParserInterface $inlineParser
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     * @param \Pimentos\DPD\Helper\Data $helperData
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Pimentos\DPD\Helper\Data $helperData
    ) {
        parent::__construct($context);
        $this->resultJsonFactory = $resultJsonFactory;
        $this->helperData = $helperData;
        $this->config = $helperData->generateDpdSdkConfig();
    }

    public function execute()
    {
        $orderId = $this->getRequest()->getParam('order_id');
        $type = $this->getRequest()->getParam('type') == 'invoice' ? 'invoice' : 'label';

        $errors = [];
        if (!$orderId) {
            $errors[] = 'Необходимо указать ID заказа';
        } else {
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $order = $objectManager->create('Magento\Sales\Model\Order')->load($orderId);

            $order = \Ipol\DPD\DB\Connection::getInstance($this->config)
                ->getTable('order')
                ->getByOrderId($order->getIncrementId());

            try {
                switch ($type) {
                    case 'invoice':
                        $response = $order->dpd()->getInvoiceFile();
                    break;

                    case 'label':
                        $labelCount = $this->getRequest()->getParam('label_count');
                        $fileFormat = $this->getRequest()->getParam('file_format');
                        $printAreaFormat = $this->getRequest()->getParam('print_area_format');
                        $response = $order->dpd()
                            ->getLabelFile($labelCount, $fileFormat, $printAreaFormat);
                    break;

                    default:
                        $result['error'] = __('Unknow action', 'dpd');
                    break;
                }
            }  catch (\SoapFault $e) {
                $errors[] = $e->getMessage();
            } catch(\Exception $e) {
                $errors[] = $e->getMessage();
            }
        }
        if (isset($response) && !$response->isSuccess()) {
            $errors = $response->getErrorMessages();
        }
        if ($errors) {
            $result['error'] = SimpleValidation::errorsHtml($errors);
        } else {
            $result['file'] = $response->getData()['file'];
        }
        return $this->resultJsonFactory
            ->create()
            ->setData($result);
    }
}