<?php

namespace Pimentos\DPD\Controller\Adminhtml\Ajax;

use Magento\Framework\App\Action\HttpGetActionInterface as HttpGetActionInterface;
use Magento\Store\Model\ScopeInterface;

class CheckImportOpportunity extends \Magento\Backend\App\Action {

    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $resultJsonFactory;

    public $scopeConfig;

    /**
     * @param Action\Context $context
     * @param \Magento\Framework\Translate\Inline\ParserInterface $inlineParser
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    ) {
        parent::__construct($context);
        $this->resultJsonFactory = $resultJsonFactory;
        $this->scopeConfig = $scopeConfig;
    }

    public function execute()
    {
        $check = 0;
        if (($this->scopeConfig
            ->getValue('common/dpd_ru/client_number_RU', ScopeInterface::SCOPE_STORE) &&
            $this->scopeConfig->getValue('common/dpd_ru/auth_key_RU', ScopeInterface::SCOPE_STORE)) ||
            ($this->scopeConfig
                ->getValue('common/dpd_kz/client_number_KZ', ScopeInterface::SCOPE_STORE) &&
            $this->scopeConfig->getValue('common/dpd_kz/auth_key_KZ', ScopeInterface::SCOPE_STORE) ||
            ($this->scopeConfig
                ->getValue('common/dpd_by/client_number_BY', ScopeInterface::SCOPE_STORE) &&
            $this->scopeConfig->getValue('common/dpd_by/auth_key_BY', ScopeInterface::SCOPE_STORE)))
        ) {
            $check = 1;
        }
        return $this->resultJsonFactory->create()->setData(['check' => $check]);
    }
}