<?php

namespace Pimentos\DPD\Controller\Adminhtml\Ajax;

use Pimentos\DPD\Helper\Autocomplete;
use Magento\Backend\App\Action;

class CitiesAutocomplete extends Action {

    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $resultJsonFactory;

    protected $config;

    /**
     * @param Action\Context $context
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     * @param \Pimentos\DPD\Helper\Data $helperData
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Pimentos\DPD\Helper\Data $helperData
    ) {
        parent::__construct($context);
        $this->resultJsonFactory = $resultJsonFactory;
        $this->config = $helperData->generateDpdSdkConfig();
    }

    public function execute()
    {
        $term = $this->getRequest()->getPost('term');
        $countryCode = $this->getRequest()->getPost('country_code') ?
            $this->getRequest()->getPost('country_code') : '';
        $term = substr($term, 0, 64);
        $cityAutocomplete = new Autocomplete($this->config);
        return $this->resultJsonFactory
            ->create()
            ->setData($cityAutocomplete->getCitiesByTerm($term, $countryCode));
    }
}