<?php

namespace Pimentos\DPD\Controller\Adminhtml\Ajax;

use Magento\Framework\App\Action\HttpGetActionInterface as HttpGetActionInterface;

use Pimentos\DPD\Helper\SimpleValidation;

class CancelDpdOrder extends \Magento\Backend\App\Action {

    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $resultJsonFactory;

    /**
     * @var \Pimentos\DPD\Helper\Data
     */
    protected $helperData;

    /**
     * @var \Pimentos\DPD\Helper\Data
     */
    protected $config;

    /**
     * @var \Magento\Tax\Api\TaxCalculationInterface
     */
    protected $taxCalculation;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @param Action\Context $context
     * @param \Magento\Framework\Translate\Inline\ParserInterface $inlineParser
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     * @param \Pimentos\DPD\Helper\Data $helperData
     * @param \Magento\Tax\Api\TaxCalculationInterface $taxCalculation
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Pimentos\DPD\Helper\Data $helperData,
        \Magento\Tax\Api\TaxCalculationInterface $taxCalculation,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    ) {
        parent::__construct($context);
        $this->resultJsonFactory = $resultJsonFactory;
        $this->helperData = $helperData;
        $this->config = $helperData->generateDpdSdkConfig();
    }

    public function execute()
    {
        $orderId = $this->getRequest()->getParam('order_id');

        $errors = [];
        if (!$orderId) {
            $errors[] = 'Необходимо указать ID заказа';
        } else {
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $order         = $objectManager->create('Magento\Sales\Model\Order')->load($orderId);

            $order = \Ipol\DPD\DB\Connection::getInstance($this->config)
                ->getTable('order')
                ->getByOrderId($order->getIncrementId());

            if ($order) {
                try {
                    $ret = $order->dpd()->cancel();
                    if (isset($ret) && !$ret->isSuccess()) {
                        foreach ($ret->getErrors() as $error) {
                            $errors[] = $error->getMessage();
                        }
                    } 
                } catch(\Exception $e) {
                    $errors[] = $error->getMessage();
                }
            } else {
                $errors[] = 'Заказ не найден.';
            }
        } 
        if ($errors) {
            $result['error'] = SimpleValidation::errorsHtml($errors);
        } else {
            $result['success'] = 'Заказ отменен';
            $statusList = \Ipol\DPD\DB\Order\Model::StatusList();
            $result['data'] = [
                'id' => '',
                'status' => $statusList['NEW']
            ];
        }
        return $this->resultJsonFactory
            ->create()
            ->setData($result);
    }
}