<?php

namespace Pimentos\DPD\Controller\Adminhtml\Ajax;

use Magento\Framework\App\Action\HttpGetActionInterface as HttpGetActionInterface;

class ImportData extends \Magento\Backend\App\Action {

    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $resultJsonFactory;

    protected $config;

    /**
     * @param Action\Context $context
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     * @param \Pimentos\DPD\Helper\Data $helperData
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Pimentos\DPD\Helper\Data $helperData
    ) {
        parent::__construct($context);
        $this->resultJsonFactory = $resultJsonFactory;
        $this->config = $helperData->generateDpdSdkConfig();
    }

    public function execute()
    {
        $error = '';
        $offset = $this->getRequest()->getPost('offset');
        $offset = $offset ? $offset : 0;
        $step = $this->getRequest()->getPost('step');
        $step = $step ? $step : 0;
        try {
            $dpdDataImport = new \Pimentos\DPD\Service\Import($this->config, $step, $offset);
            $dpdDataImport->run();
        } catch(\Exception $e) {
            $error = $e->getMessage(); 
        }
        $pecent = 0;
        if ($dpdDataImport->getTotal() && $dpdDataImport->getOffset() != -1) {
            $offset = str_replace(
                ['RU:', 'KZ:', 'BY:'],
                ['', '', ''], 
                $dpdDataImport->getOffset()
            );
            $pecent = round(
                (($offset * 100) / $dpdDataImport->getTotal()), 
                3
            );
        }
        return $this->resultJsonFactory->create()->setData([
            'step' => $dpdDataImport->getStep(),
            'stepname' => $dpdDataImport->getStepName(),
            'offset' => $dpdDataImport->getOffset(),
            'percent' => $pecent,
            'total' => $dpdDataImport->getTotal(),
            'error' => $error ? $error : ''
        ]);
    }
}