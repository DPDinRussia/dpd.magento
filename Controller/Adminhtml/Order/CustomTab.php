<?php

namespace Pimentos\DPD\Controller\Adminhtml\Order;

use Magento\Backend\App\Action;
use Magento\Sales\Api\OrderManagementInterface;
use Magento\Sales\Api\OrderRepositoryInterface;
use Psr\Log\LoggerInterface;

class CustomTab extends \Magento\Sales\Controller\Adminhtml\Order
{
    /**
     * @var \Magento\Framework\View\LayoutFactory
     */
    protected $layoutFactory;

    /**
     * @var \Pimentos\DPD\Helper\Data
     */
    protected $helperData;

    /**
     * @var \Pimentos\DPD\Helper\Data
     */
    protected $config;

    /**
     * @var \Magento\Tax\Api\TaxCalculationInterface
     */
    protected $taxCalculation;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var \Magento\Quote\Model\QuoteFactory
     */
    protected $quoteFactory;

    /**
     * @param Action\Context $context
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Magento\Framework\App\Response\Http\FileFactory $fileFactory
     * @param \Magento\Framework\Translate\InlineInterface $translateInline
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     * @param \Magento\Framework\View\Result\LayoutFactory $resultLayoutFactory
     * @param \Magento\Framework\Controller\Result\RawFactory $resultRawFactory
     * @param OrderManagementInterface $orderManagement
     * @param OrderRepositoryInterface $orderRepository
     * @param LoggerInterface $logger
     * @param \Magento\Framework\View\LayoutFactory $layoutFactory
     * @param \Pimentos\DPD\Helper\Data $helperData
     * @param \Magento\Tax\Api\TaxCalculationInterface $taxCalculation
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Quote\Model\QuoteFactory $quoteFactory
     *
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
     */
    public function __construct(
        Action\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Framework\App\Response\Http\FileFactory $fileFactory,
        \Magento\Framework\Translate\InlineInterface $translateInline,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Framework\View\Result\LayoutFactory $resultLayoutFactory,
        \Magento\Framework\Controller\Result\RawFactory $resultRawFactory,
        OrderManagementInterface $orderManagement,
        OrderRepositoryInterface $orderRepository,
        LoggerInterface $logger,
        \Magento\Framework\View\LayoutFactory $layoutFactory,
        \Pimentos\DPD\Helper\Data $helperData,
        \Magento\Tax\Api\TaxCalculationInterface $taxCalculation,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Quote\Model\QuoteFactory $quoteFactory
    ) {
        $this->layoutFactory = $layoutFactory;
        $this->helperData = $helperData;
        $this->config = $helperData->generateDpdSdkConfig();
        $this->taxCalculation = $taxCalculation;
        $this->storeManager = $storeManager;
        $this->quoteFactory = $quoteFactory;
        parent::__construct(
            $context,
            $coreRegistry,
            $fileFactory,
            $translateInline,
            $resultPageFactory,
            $resultJsonFactory,
            $resultLayoutFactory,
            $resultRawFactory,
            $orderManagement,
            $orderRepository,
            $logger
        );
    }

    /**
     * Страница заказа DPD
     *
     * @return \Magento\Framework\Controller\Result\Raw
     */
    public function execute()
    {   

        $activeTab = $this->getRequest()->getParam('dpdActiveTab') ? : 'order';

        //заказ
        $order = $this->_initOrder();

        //ищем заказ в отправленных
        $orderTabel = \Ipol\DPD\DB\Connection::getInstance($this->config)->getTable('order');
        $dpdOrder = $orderTabel->getByOrderId($order->getIncrementId());

        $dpdCreated = false;
        $sendedToDdp = false;

        if ($dpdOrder) {
            $dpdCreated  = $dpdOrder->isDpdCreated();
            $sendedToDdp = $dpdOrder->isCreated();
        }

        //адрес доставки
        $shippingAddress = $order->getShippingAddress();

        //адрес оплаты
        $billingAddress = $order->getBillingAddress();

        //способ оплаты
        $payment = $order->getPayment();
        $paymentMethod = $payment->getMethodInstance();

        //уведомления
        $notifications = [];
        $errors = [];

        /************************** Вкладка заказ ********************************/

        //статус заказа
        $status = 'NEW';

        //оплата
        $paymentType = $this->helperData->getData(
            'shipping_description/shipping_description_group/payment_type'
        );

        //служба доставки
        $deliveryType = $this->helperData->getData(
            'calculation/calculation_group/tariff_default'
        );

        //вариант доставки
        $deliveryTo = $order->getShippingMethod() == 'dpd_pickup' ? 'Т' : 'Д';
        $deliveryVariant = $this->helperData->getData(
            'shipping_description/shipping_description_group/self_pickup'
        ) ? 'Т'.$deliveryTo : 'Д'.$deliveryTo;

        //дата отправки
        $pickupDate = date('Y-m-d');

        //интервал времени передачи груза в DPD
        $pickupTimePeriod = $this->helperData->getData(
            'shipping_description/shipping_description_group/pickup_time_period'
        );

        //интервал времени доставки груза
        $deliveryTimePeriod = $this->helperData->getData(
            'shipping_description/shipping_description_group/delivery_time_period'
        );

        //габариты
        $cargoWeight = 0;
        $dimensionWidth = 0;
        $dimensionHeight = 0;
        $dimensionLength = 0;
        $cargoVolume = 0;

        //колличество грузомест(посылок)
        $cargoNumPack = $this->helperData->getData(
            'shipping_description/shipping_description_group/cargo_num_pack'
        );;

        //содержимое отправки
        $cargoCategory = $this->helperData->getData(
            'shipping_description/shipping_description_group/cargo_category'
        );

        /************************** Вкладка отправитель ***************************/
        $sender = [
            'fio' => $this->helperData->getData(
                'sender/sender_group/sender_fio'
            ),
            'name' => $this->helperData->getData(
                'sender/sender_group/sender_name'
            ),
            'phone' => $this->helperData->getData(
                'sender/sender_group/sender_phone'
            ),
            'email' => $this->helperData->getData(
                'sender/sender_group/sender_email'
            ),
            'need_pass' => $this->helperData->getData(
                'sender/sender_group/dpd_sender_need_pass'
            ),
            'street' => $this->helperData->getData(
                'sender/sender_address_to_door_group/sender_street'
            ),
            'streetabbr' => $this->helperData->getData(
                'sender/sender_address_to_door_group/sender_streetabbr'
            ),
            'house' => $this->helperData->getData(
                'sender/sender_address_to_door_group/sender_house'
            ),
            'korpus' => $this->helperData->getData(
                'sender/sender_address_to_door_group/sender_korpus'
            ),
            'str' => $this->helperData->getData(
                'sender/sender_address_to_door_group/sender_str'
            ),
            'vlad' => $this->helperData->getData(
                'sender/sender_address_to_door_group/sender_vlad'
            ),
            'office' => $this->helperData->getData(
                'sender/sender_address_to_door_group/sender_office'
            ),
            'flat' => $this->helperData->getData(
                'sender/sender_address_to_door_group/sender_flat'
            ),
            'terminal_code' => $this->helperData->getData(
                'sender/sender_group/sender_terminal_code_selected'
            ),
            'city_id' => $this->helperData->getData(
                'sender/sender_group/sender_city_id'
            ),
            'location' => $this->helperData->getData(
                'sender/sender_group/sender_city'
            )
        ];

        /************************** Вкладка получатель ***************************/
        $quote = $this->quoteFactory->create()->load($order->getQuoteId());
        $pickupPoint = $quote->getPickupPoint();

        $name = $billingAddress->getFirstName().' '.$billingAddress->getLastName();
        $recipientCity = [
            $shippingAddress->getCountryId() == 'RU' ? 'Россия' :
                ($shippingAddress->getCountryId() == 'KZ' ? 'Казахстан' : 'Беларуссия'),
            trim($shippingAddress->getRegionCode()),
            trim($shippingAddress->getCity())
        ];

        $recipient = [
            'fio' => $name,
            'name' => $name,
            'phone' => $billingAddress->getTelephone(),
            'email' => $billingAddress->getEmail(),
            'need_pass' => $this->helperData->getData(
                'recipient/recipient_group/need_pass'
            ),
            'street' => $shippingAddress->getStreet()[0],
            'streetabbr' => '',
            'house' => '',
            'korpus' => '',
            'str' => '',
            'vlad' => '',
            'office' => '',
            'flat' => '',
            'terminal_code' => $pickupPoint,
            'city_id' => '',
            'location' => implode(', ', $recipientCity),
            'terminal_code' => $pickupPoint,
        ];

        $location = new \Pimentos\DPD\Model\SDK\Location($this->config);
        $location->where(
            'ORIG_NAME LIKE :location',
            [':location' => '%'.$recipient['location'].'%'] 
        );
        $item = $location->row();
        if ($item) {
            $recipient['city_id'] = $item['CITY_ID'];
        }

        /************************** Вкладка оплата ***************************/
        $cargoValue = 0;
        $npp = 0;
        $sumNpp = 0;
        $commissionNppPayment = $this->helperData->getData(
            'calculation/calculation_group/commission_npp_payment'
        );
        $commissionNppPayment = explode(',', $commissionNppPayment);
        $useCargoValue = $this->helperData->getData(
            'calculation/calculation_group/dpd_declared_value'
        );
        if (is_array($commissionNppPayment) &&
            in_array($paymentMethod->getCode(), $commissionNppPayment)) {
            $npp = 1;
        }
        $unitLoads = [];


        /************************** Вкладка опции ***************************/
        $cargoRegistered = $this->helperData->getData(
            'shipping_description/shipping_description_options_group/cargo_registered'
        );
        $dvd = $this->helperData->getData(
            'shipping_description/shipping_description_options_group/dvd'
        );
        $trm = $this->helperData->getData(
            'shipping_description/shipping_description_options_group/trm'
        );
        $prd = $this->helperData->getData(
            'shipping_description/shipping_description_options_group/prd'
        );
        $vdo = $this->helperData->getData(
            'shipping_description/shipping_description_options_group/vdo'
        );
        $ogd = $this->helperData->getData(
            'shipping_description/shipping_description_options_group/ogd'
        );
        $esz = $this->helperData->getData(
            'shipping_description/shipping_description_options_group/esz'
        );

        //если заказ отправлялся - собираем отправленнные данные
        if ($sendedToDdp) {

            //заказ
            $status             = $dpdOrder->orderStatus;
            $paymentType        = $dpdOrder->paymentType;
            $deliveryType       = $dpdOrder->serviceCode;
            $deliveryVariant    = ''
                . ($dpdOrder->isSelfPickup()   ? 'Т' : 'Д')
                . ($dpdOrder->isSelfDelivery() ? 'Т' : 'Д')
            ;
            $pickupDate         = $dpdOrder->pickupDate;
            $pickupTimePeriod   = $dpdOrder->pickupTimePeriod;
            $deliveryTimePeriod = $dpdOrder->deliveryTimePeriod;
            $cargoWeight        = $dpdOrder->cargoWeight;
            $dimensionWidth     = $dpdOrder->dimensionWidth;
            $dimensionHeight    = $dpdOrder->dimensionHeight;
            $dimensionLength    = $dpdOrder->dimensionLength;
            $cargoVolume        = $dpdOrder->cargoVolume;
            $cargoNumPack       = $dpdOrder->cargoNumPack;
            $cargoCategory      = $dpdOrder->cargoCategory;

            //отправитель
            $sender['fio']       = $dpdOrder->senderFio;
            $sender['name']      = $dpdOrder->senderName;
            $sender['phone']     = $dpdOrder->senderPhone;
            $sender['email']     = $dpdOrder->senderEmail;
            $sender['need_pass'] = $dpdOrder->senderNeedPass;

            $location->where('ID = :id', ['id' => $dpdOrder->senderLocation]);
            $senderLocation = $location->row();
            $sender['location'] = $senderLocation ? $senderLocation->origName : 'не определено';

            $sender['street']        = $dpdOrder->senderStreet;
            $sender['streetabbr']    = $dpdOrder->senderStreetabbr;
            $sender['house']         = $dpdOrder->senderHouse;
            $sender['korpus']        = $dpdOrder->senderKorpus;
            $sender['str']           = $dpdOrder->senderStr;
            $sender['vlad']          = $dpdOrder->senderVlad;
            $sender['office']        = $dpdOrder->senderOffice;
            $sender['flat']          = $dpdOrder->senderFlat;
            $sender['terminal_code'] = $dpdOrder->senderTerminalCode;

            //получатель
            $recipient['fio']       = $dpdOrder->receiverFio;
            $recipient['name']      = $dpdOrder->receiverName;
            $recipient['phone']     = $dpdOrder->receiverPhone;
            $recipient['email']     = $dpdOrder->receiverEmail;
            $recipient['need_pass'] = $dpdOrder->receiverNeedPass;

            $location->where('ID = :id', ['id' => $dpdOrder->receiverLocation]);
            $receiverLocation = $location->row();
            $recipient['location'] = $receiverLocation ? $receiverLocation->origName : 'не определенно';

            $recipient['street']        = $dpdOrder->receiverStreet;
            $recipient['streetabbr']    = $dpdOrder->receiverStreetabbr;
            $recipient['house']         = $dpdOrder->receiverHouse;
            $recipient['korpus']        = $dpdOrder->receiverKorpus;
            $recipient['str']           = $dpdOrder->receiverStr;
            $recipient['vlad']          = $dpdOrder->receiverVlad;
            $recipient['office']        = $dpdOrder->receiverOffice;
            $recipient['flat']          = $dpdOrder->receiverFlat;
            $recipient['terminal_code'] = $dpdOrder->receiverTerminalCode;

            //оплата
            $unitLoads       = $dpdOrder->getUnitLoads();
            $useCargoValue   = $dpdOrder->useCargoValue == 'Y';
            $cargoValue      = $dpdOrder->cargoValue;
            $npp             = $dpdOrder->npp == 'Y';
            $sumNpp          = $dpdOrder->sumNpp;

            //опции
            $cargoRegistered = $dpdOrder->cargoRegistered == 'Y' ? 1 : 0;
            $dvd = $dpdOrder->dvd == 'Y' ? 1 : 0;
            $trm = $dpdOrder->trm == 'Y' ? 1 : 0;
            $prd = $dpdOrder->prd == 'Y' ? 1 : 0;
            $vdo = $dpdOrder->vdo == 'Y' ? 1 : 0;
            $ogd = $dpdOrder->ogd;
            $esz = $dpdOrder->esz;
        }

        $changedOrder = $this->getRequest()->getPost('order');

        //изменение данных пользователем
        if ($_SERVER['REQUEST_METHOD'] === 'POST' && $changedOrder) {
            $changedOrder = $this->getRequest()->getPost('order');
            foreach ($changedOrder as $key => $value) {
                if ($key == 'unit_loads') {
                    continue;
                }
                ${$key} = $value;
            }
            if (!$deliveryVariant) {
                $deliveryVariant = 'ДД';
            }

            if (($deliveryVariant == 'ТД' || $deliveryVariant == 'ТТ')
                && !isset($sender['terminal_code'])
            ) {
                $sender['terminal_code'] = $this->helperData->getData(
                    'sender/sender_group/sender_terminal_code_selected'
                );
            }

            if (($deliveryVariant == 'ДТ' || $deliveryVariant == 'ТТ')
                && !isset($recipient['terminal_code'])
            ) {
                $recipient['terminal_code'] = $pickupPoint;
            }

            if (!isset($sender['need_pass'])) {
                $sender['need_pass'] = '';
            }
            if (!isset($recipient['need_pass'])) {
                $recipient['need_pass'] = '';
            }

            
            $sender['city'] = $this->helperData->getData(
                'sender/sender_group/sender_city'
            );

            //оплата
            $useCargoValue = isset($changedOrder['useCargoValue']) ? : 0;
            $cargoValue    = 0;
            $npp           = isset($changedOrder['npp']) ? : 0;
            $sumNpp        = 0;

            //опции
            $cargoRegistered = isset($changedOrder['cargoRegistered']) ? : 0;
            $dvd = isset($changedOrder['dvd']) ? : 0;
            $trm = isset($changedOrder['trm']) ? : 0;
            $prd = isset($changedOrder['prd']) ? : 0;
            $vdo = isset($changedOrder['vdo']) ? : 0;
        }

        //создаем конвертер
        $converter = new \Pimentos\DPD\Helper\Converter();

        //создаем отправку
        $shipmentFactory = new \Pimentos\DPD\Builder\Shipment(
            $this->config,
            $this->helperData->getData(
                'sender/sender_group/sender_city'
            ),
            $shippingAddress->getCountryId(),
            $shippingAddress->getRegionCode(),
            $shippingAddress->getCity(),
            $order->getSubtotal(),
            $order->getAllVisibleItems(),
            $this->taxCalculation
        );
        $shipmentFactory->setDeliveryVariant($deliveryVariant);
        $shipment = $shipmentFactory->getInstance();

        //задаем способ оплаты
        $shipment->setPaymentMethod(1, $paymentMethod->getCode());

        //задаем габариты из позиций заказа
        if (!$cargoWeight) {
            $cargoWeight = $shipment->getWeight();
        }
        if (!$dimensionWidth) {
            $dimensionWidth = $shipment->getWidth();
        }
        if (!$dimensionHeight) {
            $dimensionHeight = $shipment->getHeight();
        }
        if (!$dimensionLength) {
            $dimensionLength = $shipment->getLength();
        }
        if (!$cargoVolume) {
            $cargoVolume = $shipment->getVolume();
        }
        $shipment->setDimensions(
           $dimensionWidth,
           $dimensionHeight,
           $dimensionLength,
           $cargoWeight
        );

        //расчитываем стоимость доставки
        $calc = $shipment->calculator();
        $calc->setCurrencyConverter($converter);
        $tariff = [];
        try {

            //считаем
            $tariff = $calc->calculate(
                $this->storeManager->getStore()->getBaseCurrencyCode()
            );

            //округляем
            $tariff = \Pimentos\DPD\Helper\PriceRules::round(
                $tariff,
                $this->helperData->getData(
                    'calculation/calculation_group/round_to'
                )
            );

            $increaseDeliveryTime = $this->helperData->getData(
                'calculation/calculation_group/add_delivery_day'
            );
            
            if ($increaseDeliveryTime) {
                $tariff['DAYS'] += $increaseDeliveryTime;
            }

            //unit loads
            if (!$sendedToDdp) {
                $cartItems = [];
                foreach ($order->getAllVisibleItems() as $item) {
                    $itemChildren = $item->getChildren();
                    if ($item->getHasChildren() && !empty($item->getChildren())) {
                        foreach ($itemChildren as $child) {
                            $cartItems[] = $child;
                        }
                    } else {
                        $cartItems[] = $item;
                    }
                }
                $dpdItems = [];
                $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
                $productObject = $objectManager->create('Magento\Catalog\Model\Product');

                foreach ($cartItems as $item) {
                    $product = $productObject->load($item->getProductId());

                    $declaredPrice = 0;
                    $nppPrice = 0;
                    $productQty = (int)$item->getQtyOrdered();
                    $tax = '';

                    if ($useCargoValue || $npp) {
                        $productPrice = $item->getPrice();
                        if (!intval($productPrice)) {
                            $parent = $productObject->load(
                                $product->getTypeInstance()->getParentIdsByChild($item->getProductId())
                            );
                            if ($parent) {
                                $productPrice = $parent->getPrice();
                            }
                        }
                        $ulPosition = [];
                        if (isset($changedOrder['unit_loads'])) {
                            foreach ($changedOrder['unit_loads'] as $ul) {
                                if ($product->getName() == $ul['NAME']) {
                                    $ulPosition = $ul; 
                                    break;
                                }
                            }
                            if ($ulPosition) {
                                $productQty = $ulPosition['QUANTITY'];
                                $tax = $ulPosition['VAT'];
                            }
                        }

                        if ($useCargoValue) {
                            $declaredPrice = isset($ulPosition['CARGO']) ?
                                $ulPosition['CARGO'] : $productPrice;
                        }
                        if ($npp) {
                            $nppPrice = isset($ulPosition['NPP']) ?
                                $ulPosition['NPP'] : $productPrice;
                        }
                    }

                    $unitLoads[crc32($product->getName())] = [
                        'NAME'     => $product->getName(),
                        'QUANTITY' => $productQty,
                        'CARGO'    => round($declaredPrice, 2),
                        'NPP'      => round($nppPrice, 2),
                        'VAT'      => $tax
                    ];

                    $cargoValue += $declaredPrice * $productQty;
                    $sumNpp += $nppPrice * $productQty;
                }

                $ulDelivery = [];
                if (isset($changedOrder['unit_loads'])) {
                    foreach ($changedOrder['unit_loads'] as $ul) {
                        if ($ul['NAME'] == 'Доставка') {
                            $ulDelivery = $ul; 
                            break;
                        }
                    }
                }

                if ($paymentType != \Ipol\DPD\Order::PAYMENT_TYPE_OUP) {
                    if ($npp) {
                        $deliveryNppCost = $ulDelivery && !empty($ulDelivery['NPP']) ? $ulDelivery['NPP'] : $tariff['COST'];                    
                    } else {
                        $deliveryNppCost = 0;

                    }
                    $unitLoads['DELIVERY'] = [
                        'NAME'     => 'Доставка',
                        'QUANTITY' => $ulDelivery ? (int)$ulDelivery['QUANTITY'] : 1,
                        'CARGO'    => 0,
                        'NPP'      => $deliveryNppCost,
                        'VAT'      => $ulDelivery ? $ulDelivery['VAT'] : '',
                    ];

                    $sumNpp += $deliveryNppCost;
                }
            }

        } catch(\SoapFault $e) {
            $errors[] = $e->getMessage();
        } catch(\Exception $e) {
            $errors[] = $e->getMessage();
        }

        $senderTerminals = [];
        $recipientTerminals = [];
        
        //проверяем терминалы на доступность для получателя и для отправителя
        $terminal = new \Pimentos\DPD\Model\SDK\Terminal($this->config);

        if ($deliveryVariant == 'ТТ' || $deliveryVariant == 'ТД') {    
            //отправитель
            $terminal->where(
                'LOCATION_ID = :location_id AND SCHEDULE_SELF_PICKUP != ""',
                [':location_id' => $sender['city_id']]);
            $senderTerminals = $terminal->get();

            $senderTerminals = \Pimentos\DPD\Model\SDK\Terminal::onlyAvaliableByDimessions(
                $senderTerminals,
                $shipment
            );
            if (!count($senderTerminals)) {
                $errors[] = 'Не найдено терминалов по заданным габаритам';
            } else {
                if (!\Pimentos\DPD\Model\SDK\Terminal::checkSelectedTerminalOnExists(
                    $senderTerminals,
                    $sender['terminal_code'])
                ) {
                    $notifications[] = 'Терминал отправителя был изменен';
                }
            }
        }

        if ($deliveryVariant == 'ДТ' || $deliveryVariant == 'ТТ') {
            //получатель
            $where = 'LOCATION_ID = :location_id AND SCHEDULE_SELF_DELIVERY != ""';
            $bind = [':location_id' => $recipient['city_id']];
            if ($ogd) {
                $where .= ' AND SERVICES LIKE :ogd';
                $bind['ogd'] = '%'.$ogd.'%';
            }
            $terminal->where($where, $bind);
            $recipientTerminals = $terminal->get();
            $recipientTerminals = \Pimentos\DPD\Model\SDK\Terminal::onlyAvaliable(
                $recipientTerminals,
                $shipment
            );
            if ($sumNpp) {
                $recipientTerminals = \Pimentos\DPD\Model\SDK\Terminal::onlyAvaliableNpp(
                    $recipientTerminals,
                    $shipment,
                    $sumNpp
                );
            }
            if (!count($recipientTerminals)) {
                $errors[] = 'Не найдено терминалов по заданным настройкам для получателя';
            } else {
                if (!\Pimentos\DPD\Model\SDK\Terminal::checkSelectedTerminalOnExists(
                    $recipientTerminals,
                    $recipient['terminal_code'])
                ) {
                    $notifications[] = 'Терминал получателя был изменен';
                }
            }
        }

        $statusList = \Ipol\DPD\DB\Order\Model::StatusList();
        $status = isset($statusList[$status]) ? $statusList[$status] : 'unknow';

        //собираем и выводим вьюс
        $layout = $this->layoutFactory->create();
        $html = $layout->createBlock('Pimentos\DPD\Block\Adminhtml\Order\View\Tab\Custom')
            ->setData('notifications', $notifications)
            ->setData('errors', $errors)
            ->setData('activeTab', $activeTab)
            ->setData('status', $status)
            ->setData('orderId', $order->getId())
            ->setData('orderIncrementId', $order->getIncrementId())
            ->setData('orderNum', $dpdOrder ? $dpdOrder->orderNum : '')
            ->setData('paymentType', $paymentType)
            ->setData('deliveryType', $deliveryType)
            ->setData('pickupDate', $pickupDate)
            ->setData('pickupTimePeriod', $pickupTimePeriod)
            ->setData('deliveryTimePeriod', $deliveryTimePeriod)
            ->setData('cargoWeight', $cargoWeight)
            ->setData('dimensionWidth', $dimensionWidth)
            ->setData('dimensionHeight', $dimensionHeight)
            ->setData('dimensionLength', $dimensionLength)
            ->setData('cargoVolume', $cargoVolume)
            ->setData('cargoNumPack', (int)$cargoNumPack)
            ->setData('cargoCategory', $cargoCategory)
            ->setData('sender', $sender)
            ->setData('senderTerminals', $senderTerminals)
            ->setData('recipient', $recipient)
            ->setData('recipientTerminals', $recipientTerminals)
            ->setData('unitLoads', $unitLoads)
            ->setData('useCargoValue', $useCargoValue)
            ->setData('cargoValue', $cargoValue)
            ->setData('cargoRegistered', $cargoRegistered)
            ->setData('npp', $npp)
            ->setData('sumNpp', round($sumNpp, 2))
            ->setData('dvd', $dvd)
            ->setData('trm', $trm)
            ->setData('prd', $prd)
            ->setData('vdo', $vdo)
            ->setData('ogd', $ogd)
            ->setData('esz', $esz)
            ->setData('tariff', $tariff)
            ->setData('sendedToDdp', $sendedToDdp)
            ->setData('deliveryVariant', $deliveryVariant)
            ->setData('currencyCode',  $this->storeManager->getStore()->getBaseCurrencyCode())
            ->setData('dpdCreated',  $dpdCreated)
            ->toHtml();
        $this->_translateInline->processResponseBody($html);
        /** @var \Magento\Framework\Controller\Result\Raw $resultRaw */
        $resultRaw = $this->resultRawFactory->create();
        $resultRaw->setContents($html);
        return $resultRaw;
    }
}