<?php

namespace Pimentos\DPD\Helper;

class PriceRules {

    /**
     * Округление цены
     * @param array $tariff
     * @param integer $roundTo
     * @return $tariff
     */
    public static function round($tariff, $roundTo)
    {
        if (intval($roundTo)) {
            $tariff['COST'] = ceil($tariff['COST'] / $roundTo) * $roundTo;
        } else {
            $tariff['COST'] = round($tariff['COST'], 2);
        }
        return $tariff;
    }
}