<?php

namespace Pimentos\DPD\Helper;

 class Converter implements \Ipol\DPD\Currency\ConverterInterface {

    public function convert($amount, $currencyFrom, $currencyTo, $actualDate = false)
    {
        if ($actualDate) {
            $actualDate = date('d/m/Y', strtotime($actualDate));
        }
        
        $cachePath          = $_SERVER['DOCUMENT_ROOT'].'/var/cache/'.hash('sha256','currency'.$actualDate);
        $currencyRatesData  = [];
        $forOneCurrencyUnit = 1;

        if (file_exists($cachePath)) {
            $currencyRatesData = simplexml_load_string(file_get_contents($cachePath));
        }
        if (!$currencyRatesData || (time() - filemtime($cachePath)) > 86400) { //сутки кэш
            try {
                $xml = file_get_contents(
                    'http://www.cbr.ru/scripts/XML_daily.asp'.
                    ($actualDate ? '?date_req='.$actualDate : '')
                );
                $currencyRatesData = simplexml_load_string($xml);
                file_put_contents($cachePath, $xml);
            } catch (\Exception $e) {}
        }
        if ($currencyRatesData) {
            $from = [
                'currency' => $currencyFrom,
                'nominal' => 1,
                'value' => 1
            ];
            $to = [
                'currency' => $currencyTo,
                'nominal' => 1,
                'value' => 1
            ];
            foreach ($currencyRatesData->Valute as $item) {
                if ($item->CharCode == $currencyFrom) {
                    $from['nominal'] = str_replace(',', '.', $item->Nominal);
                    $from['value'] = str_replace(',', '.', $item->Value);
                }
                if ($item->CharCode == $currencyTo) {
                    $to['nominal'] = str_replace(',', '.', $item->Nominal);
                    $to['value'] = str_replace(',', '.', $item->Value);
                }
            }

            $forOneCurrencyUnit = round((
                ($from['nominal'] / $from['value']) / ($to['nominal'] / $to['value'])
            ), 2);
        }
        return $amount / $forOneCurrencyUnit;
    }

 }