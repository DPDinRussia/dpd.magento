<?php

namespace Pimentos\DPD\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Filesystem\DirectoryList;

class Data extends AbstractHelper
{

    protected $deploymentConfig;

    protected $scopeConfig;

    /**
     * @param Action\Context 
     */
    public function __construct(
        \Magento\Framework\App\DeploymentConfig $deploymentConfig,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\Setup\SchemaSetupInterface $installer
    ) {
        $this->deploymentConfig = $deploymentConfig;
        $this->scopeConfig = $scopeConfig;
        $this->installer = $installer;
    }


    public function getData($key)
    {
        return $this->scopeConfig
            ->getValue(
                $key,
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    public function getDpdSdkConfigArray()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $filesystem = $objectManager->create('Magento\Framework\Filesystem');
        $dpdTarifOff = $this->scopeConfig
            ->getValue(
                'calculation/calculation_group/ignore_tariff',
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
        $nppPayment = $this->scopeConfig
            ->getValue(
                'calculation/calculation_group/commission_npp_payment',
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
        $nppPayment = explode(',', $nppPayment);

        return [
            'KLIENT_NUMBER' => $this->scopeConfig
                ->getValue(
                    'common/dpd_ru/client_number_RU',
                    \Magento\Store\Model\ScopeInterface::SCOPE_STORE
            ),
            'KLIENT_KEY' => $this->scopeConfig
                ->getValue(
                    'common/dpd_ru/auth_key_RU',
                    \Magento\Store\Model\ScopeInterface::SCOPE_STORE
            ),
            'KLIENT_CURRENCY' => $this->scopeConfig
                ->getValue(
                    'common/dpd_ru/currency_RU',
                    \Magento\Store\Model\ScopeInterface::SCOPE_STORE
            ),
            'KLIENT_NUMBER_KZ' => $this->scopeConfig
                ->getValue(
                    'common/dpd_kz/client_number_KZ',
                    \Magento\Store\Model\ScopeInterface::SCOPE_STORE
            ),
            'KLIENT_KEY_KZ' => $this->scopeConfig
                ->getValue(
                    'common/dpd_kz/auth_key_KZ',
                    \Magento\Store\Model\ScopeInterface::SCOPE_STORE
            ),
            'KLIENT_CURRENCY_KZ' => $this->scopeConfig
                ->getValue(
                    'common/dpd_kz/currency_KZ',
                    \Magento\Store\Model\ScopeInterface::SCOPE_STORE
            ),
            'KLIENT_NUMBER_BY' => $this->scopeConfig
                ->getValue(
                    'common/dpd_by/client_number_BY',
                    \Magento\Store\Model\ScopeInterface::SCOPE_STORE
            ),
            'KLIENT_KEY_BY' => $this->scopeConfig
                ->getValue(
                    'common/dpd_by/auth_key_BY',
                    \Magento\Store\Model\ScopeInterface::SCOPE_STORE
            ),
            'KLIENT_CURRENCY_BY' => $this->scopeConfig
                ->getValue(
                    'common/dpd_by/currency_BY',
                    \Magento\Store\Model\ScopeInterface::SCOPE_STORE
            ),
            'IS_TEST' => $this->scopeConfig
                ->getValue(
                    'common/common_group/test_mode',
                    \Magento\Store\Model\ScopeInterface::SCOPE_STORE
            ),
            'WEIGHT' => $this->scopeConfig
                ->getValue(
                    'dimensions/dimensions_group/weight_default',
                    \Magento\Store\Model\ScopeInterface::SCOPE_STORE
            ),
            'LENGTH' => $this->scopeConfig
                ->getValue(
                    'dimensions/dimensions_group/length_default',
                    \Magento\Store\Model\ScopeInterface::SCOPE_STORE
            ),
            'WIDTH' => $this->scopeConfig
                ->getValue(
                    'dimensions/dimensions_group/width_default',
                    \Magento\Store\Model\ScopeInterface::SCOPE_STORE
            ),
            'HEIGHT' => $this->scopeConfig
                ->getValue(
                    'dimensions/dimensions_group/height_default',
                    \Magento\Store\Model\ScopeInterface::SCOPE_STORE
            ),
            'TARIFF_OFF' => $dpdTarifOff,
            'DEFAULT_TARIFF_CODE' => $this->scopeConfig
                ->getValue(
                    'calculation/calculation_group/tariff_default',
                    \Magento\Store\Model\ScopeInterface::SCOPE_STORE
            ),
            'DEFAULT_TARIFF_THRESHOLD' => $this->scopeConfig
                ->getValue(
                    'calculation/calculation_group/default_tariff_treshold',
                    \Magento\Store\Model\ScopeInterface::SCOPE_STORE
            ),
            'DECLARED_VALUE' => $this->scopeConfig
                ->getValue(
                    'calculation/calculation_group/dpd_declared_value',
                    \Magento\Store\Model\ScopeInterface::SCOPE_STORE
            ),
            'COMMISSION_NPP_CHECK' => [
                1 => $this->scopeConfig
                    ->getValue(
                        'calculation/calculation_group/commission_npp_check',
                        \Magento\Store\Model\ScopeInterface::SCOPE_STORE
                ) ? 1 : 0,
                2 => false
            ],
            'COMMISSION_NPP_PERCENT' => [
                1 => $this->scopeConfig
                    ->getValue(
                        'calculation/calculation_group/commission_npp_percent',
                        \Magento\Store\Model\ScopeInterface::SCOPE_STORE
                ),
                2 => 0
            ],
            'COMMISSION_NPP_MINSUM' => [
                1 =>  $this->scopeConfig
                    ->getValue(
                        'calculation/calculation_group/commission_npp_minsum',
                        \Magento\Store\Model\ScopeInterface::SCOPE_STORE
                ),
                2 => 0
            ],
            'COMMISSION_NPP_PAYMENT' => [
                1 => $nppPayment,
                2 => []
            ],
            'REQUIRED_PICKPOINT_SELECTION' => $this->scopeConfig
                ->getValue(
                    'common/common_group/required_pickpoint_selection',
                    \Magento\Store\Model\ScopeInterface::SCOPE_STORE
            ),
            'UPLOAD_DIR' => $filesystem->getDirectoryRead(DirectoryList::MEDIA)->getAbsolutePath(),

            'DB' => [
                'DSN'      => 'mysql:host='. 
                    $this->deploymentConfig->get('db/connection/default/host').';dbname='.
                    $this->deploymentConfig->get('db/connection/default/dbname'),
                'USERNAME' => $this->deploymentConfig->get('db/connection/default/username'),
                'PASSWORD' => $this->deploymentConfig->get('db/connection/default/password'),
                'DRIVER'   => null,
                'PDO'      => null,
                'TABLES'   => [
                    'location' => $this->installer->getTable('b_ipol_dpd_location'),
                    'terminal' => $this->installer->getTable('b_ipol_dpd_terminal'),
                    'order'    => $this->installer->getTable('b_ipol_dpd_order'),
                ],
            ],

            'SOURCE_NAME' => 'Magento',
        ];
    }

    public function generateDpdSdkConfig()
    {
        return new \Ipol\DPD\Config\Config($this->getDpdSdkConfigArray());
    }

}