<?php

namespace Pimentos\DPD\Helper;

use Magento\Framework\App\Helper\AbstractHelper;

class Autocomplete extends AbstractHelper
{
    private $config;

    public function __construct(\Ipol\DPD\Config\Config $config)
    {
        $this->config = $config;
    }

    public function getCitiesByTerm($term, $countryCode = 'RU')
    {
        $result = [];

        if ($term) {
            $db = \Ipol\DPD\DB\Connection::getInstance($this->config);

            $locationTable = $db->getTable('location');
            $find = [
                'where' => 'CITY_NAME LIKE :city',
                'limit' => '0,10',
                'bind'  => [':city' => mb_strtolower($term) .'%'],
                'order' => 'IS_CITY DESC, CITY_NAME ASC',
            ];

            if ($countryCode) {
                $find['where'] .= ' AND COUNTRY_CODE = :contry_code';
                $find['bind'][':contry_code'] =  $countryCode;
            }
            $items = $locationTable->find($find);
            while($item = $items->fetch()) {
                $name = explode(',', $item['ORIG_NAME']);
                $result[] = [
                    'id'     => $item['CITY_ID'],
                    'value'  => $countryCode ? trim($name[2]).', '.trim($name[1]) : $item['ORIG_NAME'],
                    'city'   => $name[2],
                    'region' => $item['REGION_NAME']
                ];
            }
        }
        return $result;
    }
}