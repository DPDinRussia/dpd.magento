/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
    'jquery',
    'ko',
    'Magento_Checkout/js/model/checkout-data-resolver',
    'Magento_Checkout/js/model/quote',
    'api-maps.yandex',
    'jquery.dpd.map'
], function ($, ko, checkoutDataResolver, quote) {
    'use strict';

    var shippingRates = ko.observableArray([]);

    return {

        inited: false,

        isLoading: ko.observable(false),

        /**
         * Set shipping rates
         *
         * @param {*} ratesData
         */
        setShippingRates: function (ratesData) {
            this.inited = false;
            this.destroyMap();
            var ths = this;
            shippingRates(ratesData);
            shippingRates.valueHasMutated();
            checkoutDataResolver.resolveShippingRates(ratesData);
            if (!$('#change_dpd_pickup').length) {
                var selectPickUpText = $('#label_carrier_pickup_dpd').html()
                    + '<br><a href="#" id="change_dpd_pickup">Выбрать пвз</a>';
                $('#label_carrier_pickup_dpd').html(selectPickUpText);
                $('#change_dpd_pickup').click(function(e){
                    e.preventDefault();
                    if (!$('.dpd-overlay').length) {
                        ths.loadPickups();
                    } else {
                        ths.showMap();
                    }
                });
            }
        },

        /**
         * Get shipping rates
         *
         * @returns {*}
         */
        getShippingRates: function () {
            return shippingRates;
        },

        loadPickups: function() {
            if (this.inited) {
                return;
            }
            var shippingAddress = quote.shippingAddress();
            var ths = this;
            $.ajax({
                url: '/dpd/front/pickups',
                dataType: 'json',
                type: 'post',
                data: {
                  country_code: shippingAddress.countryId,
                  region: shippingAddress.region,
                  city: shippingAddress.city
                },
                beforeSend: function(){
                    $(document.body).append(
                        '<div class="dpd-overlay"></div>'+
                        '<div class="dpd-popup">'+
                        '<img class="loader" src="' + 
                            require.toUrl('Pimentos_DPD/images/ajax-loader.gif') + '">'+
                        '<div id="pimentos_dpd_map"></div>'+
                        '<a href="#" class="dpd-close">&#10005;</a>'+
                        '</div>'
                    );
                },
                success: function(response) {
                    $('#pimentos_dpd_info').text('Выберите Ваш пункт самовывоза:');
                    ths.initMapWidget(response);
                }
            });
            if ($.cookie('dpd_payment_method')) {
                window.localStorage.setItem('dpd_npp_margin_calculated', 1);
            } else {
                window.localStorage.setItem('dpd_npp_margin_calculated', 0);
            }
        },

        initMapWidget: function(data){
            if (this.inited) {
                return;
            }
            var ths = this;
            $('.dpd-popup .loader').hide();
            $('.dpd-close').show();
            $('#pimentos_dpd_map').dpdMap({
                placemarkIcon : require.toUrl('Pimentos_DPD/images/pickup_locationmarker.png'),
                placemarkIconActive :
                    require.toUrl('Pimentos_DPD/images/pickup_locationmarker_highlighted.png'),
            }, data)
            .on('dpd.map.terminal.select', function(e, terminal, widget) {
                $('[name="street[0]"]').val(terminal.NAME).keyup();
                $('input[name="pickup_point"]').val(terminal.CODE);
                if (!terminal.SCHEDULE_PAYMENT_CASH &&
                    !terminal.SCHEDULE_PAYMENT_CASHLESS) {
                    window.localStorage.setItem('dpd_is_npp_pickup', 0);
                } else {
                    window.localStorage.setItem('dpd_is_npp_pickup', 1);
                }
                $('#label_carrier_pickup_dpd').html('Самовывоз<br><a href="#" id="change_dpd_pickup">' + 
                    terminal.NAME + '</a>');
                ths.hideMap();
            });

            $(document.body).on('click', '.dpd-close', function(e){
                e.preventDefault();
                ths.hideMap();
            });

            $(document.body).on('click', '#change_dpd_pickup', function(e){
                e.preventDefault();
                ths.showMap();
            });

            this.inited = true;
        },

        hideMap: function() {
            $('.dpd-overlay').hide();
            $('.dpd-popup').hide(); 
        },

        showMap: function() {
            $('.dpd-overlay').show();
            $('.dpd-popup').show(); 
        },

        destroyMap: function() {
            $('.dpd-overlay').fadeOut('fast', function(){
                $('.dpd-popup').remove();
                $('.dpd-overlay').remove(); 
            });
            this.inited = false;
        }
    };
});
