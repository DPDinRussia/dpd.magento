require([
    'jquery',
    'jquery/ui',
    'domReady!'
    ], function($, ui){

        //из-за knockout.js приходится писать костыль
        var checkountRenderTimerId = setInterval(function(){
            if (!$('#checkout-loader').length) {
                $('#checkout').trigger('dpd:checkout:rendered');
            }
        }, 100);

        $('#checkout').on('dpd:checkout:rendered', function(){
            clearInterval(checkountRenderTimerId);
            
            $('select[name="country_id"]').change(function(){
                $('input[name="city"]').val('');
            })

            $('input[name="city"]').autocomplete({
                source: function(request, response) {
                    var countryId = $('select[name="country_id"]').val();
                    $.ajax({
                        url: '/dpd/front/citiesautocomplete',
                        type: 'post',
                        dataType: 'json',
                        data: {
                          term: request.term,
                          country_code: countryId
                        },
                        success: function(data) {
                          response(data);
                        }
                    });
                },
                minLength: 3,
                select: function(event, ui) {
                  $('input[name="city"]').val(ui.item.city);
                  $('input[name="region"]').val(ui.item.region);
                  $('input[name="region"]').trigger('change');
                  return false;
                }
              });
        });
});