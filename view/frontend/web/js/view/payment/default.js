/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
    'ko',
    'jquery',
    'uiComponent',
    'Magento_Checkout/js/action/place-order',
    'Magento_Checkout/js/action/select-payment-method',
    'Magento_Checkout/js/model/quote',
    'Magento_Customer/js/model/customer',
    'Magento_Checkout/js/model/payment-service',
    'Magento_Checkout/js/checkout-data',
    'Magento_Checkout/js/model/checkout-data-resolver',
    'uiRegistry',
    'Magento_Checkout/js/model/payment/additional-validators',
    'Magento_Ui/js/model/messages',
    'uiLayout',
    'Magento_Checkout/js/action/redirect-on-success',
    'Magento_Ui/js/model/messageList',
    'Magento_Checkout/js/model/step-navigator',
    'Magento_Checkout/js/model/shipping-rate-registry',
    'jquery/jquery.cookie'
], function (
    ko,
    $,
    Component,
    placeOrderAction,
    selectPaymentMethodAction,
    quote,
    customer,
    paymentService,
    checkoutData,
    checkoutDataResolver,
    registry,
    additionalValidators,
    Messages,
    layout,
    redirectOnSuccessAction,
    globalMessageList,
    stepNavigator,
    rateRegistry
) {
    'use strict';

    return Component.extend({
        redirectAfterPlaceOrder: true,
        isPlaceOrderActionAllowed: ko.observable(quote.billingAddress() != null),

        /**
         * After place order callback
         */
        afterPlaceOrder: function () {
            // Override this function and put after place order logic here
        },

        /**
         * Initialize view.
         *
         * @return {exports}
         */
        initialize: function () {
            var billingAddressCode,
                billingAddressData,
                defaultAddressData;

            this._super().initChildren();
            quote.billingAddress.subscribe(function (address) {
                this.isPlaceOrderActionAllowed(address !== null);
            }, this);
            checkoutDataResolver.resolveBillingAddress();

            billingAddressCode = 'billingAddress' + this.getCode();
            registry.async('checkoutProvider')(function (checkoutProvider) {
                defaultAddressData = checkoutProvider.get(billingAddressCode);

                if (defaultAddressData === undefined) {
                    // Skip if payment does not have a billing address form
                    return;
                }
                billingAddressData = checkoutData.getBillingAddressFromData();

                if (billingAddressData) {
                    checkoutProvider.set(
                        billingAddressCode,
                        $.extend(true, {}, defaultAddressData, billingAddressData)
                    );
                }
                checkoutProvider.on(billingAddressCode, function (providerBillingAddressData) {
                    checkoutData.setBillingAddressFromData(providerBillingAddressData);
                }, billingAddressCode);
            });

            return this;
        },

        /**
         * Initialize child elements
         *
         * @returns {Component} Chainable.
         */
        initChildren: function () {
            this.messageContainer = new Messages();
            this.createMessagesComponent();

            return this;
        },

        /**
         * Create child message renderer component
         *
         * @returns {Component} Chainable.
         */
        createMessagesComponent: function () {

            var messagesComponent = {
                parent: this.name,
                name: this.name + '.messages',
                displayArea: 'messages',
                component: 'Magento_Ui/js/view/messages',
                config: {
                    messageContainer: this.messageContainer
                }
            };

            layout([messagesComponent]);

            return this;
        },

        /**
         * Place order.
         */
        placeOrder: function (data, event) {
            var self = this;

            if (event) {
                event.preventDefault();
            }

            if (this.validate() && additionalValidators.validate()) {

                if (!this.checkNPP()) {
                    return false;
                }

                this.isPlaceOrderActionAllowed(false);

                this.getPlaceOrderDeferredObject()
                    .fail(
                        function () {
                            self.isPlaceOrderActionAllowed(true);
                        }
                    ).done(
                        function () {
                            self.afterPlaceOrder();

                            if (self.redirectAfterPlaceOrder) {
                                redirectOnSuccessAction.execute();
                            }
                        }
                    );

                return true;
            }

            return false;
        },

        /**
         * @return {*}
         */
        getPlaceOrderDeferredObject: function () {
            return $.when(
                placeOrderAction(this.getData(), this.messageContainer)
            );
        },

        /**
         * @return {Boolean}
         */
        selectPaymentMethod: function () {
            selectPaymentMethodAction(this.getData());
            checkoutData.setSelectedPaymentMethod(this.item.method);

            this.checkNPP();

            return true;
        },

        checkNPP: function(){
            if ((checkoutData.getSelectedShippingRate() == 'dpd_pickup' || 
                checkoutData.getSelectedShippingRate() == 'dpd_courier') &&
                $.inArray(
                    checkoutData.getSelectedPaymentMethod(),
                    window.checkoutConfig.COMMISSION_NPP_PAYMENT[1]
                ) !== -1
            ) {
                $.cookie('dpd_payment_method', checkoutData.getSelectedPaymentMethod());
                if (checkoutData.getSelectedShippingRate() == 'dpd_courier' &&
                    (window.localStorage.getItem('dpd_npp_margin_calculated') == 0 ||
                        window.localStorage.getItem('dpd_npp_margin_calculated') == null)
                ) {
                    window.localStorage.setItem('dpd_npp_margin_calculated', 1);
                    globalMessageList.addSuccessMessage({ 
                        message: 'Вы выбрали оплату наложенным платежом. '+
                            'Подтвердите новую стоимость доставки. Идет переадресация...'

                    });
                    $('html, body').animate({
                        scrollTop: $('body').offset().top
                    });
                    setTimeout(function(){
                        stepNavigator.navigateTo('shipping');
                    }, 3000);

                    this.recalcShipping();
                    return false;
                } else {
                    if (window.localStorage.getItem('dpd_is_npp_pickup') == 0) {
                        globalMessageList.addErrorMessage({ 
                            message: 'Выбранный пункт самовывоза DPD не '+
                            'поддерживает оплату наложенным платежом. ' +
                            'Выберите другой пункт или способ оплаты.'

                        });
                        $('html, body').animate({
                            scrollTop: $('body').offset().top
                        });
                        return false;
                    } else if (window.localStorage.getItem('dpd_npp_margin_calculated') == 0) {
                        globalMessageList.addSuccessMessage({ 
                            message: 'Вы выбрали оплату наложенным платежом. '+
                                'Подтвердите новую стоимость доставки. Идет переадресация...'

                        });
                        $('html, body').animate({
                            scrollTop: $('body').offset().top
                        });


                        $('input[name="pickup_point"]').val('');
                        
                        setTimeout(function(){
                            stepNavigator.navigateTo('shipping');
                        }, 3000);

                        this.recalcShipping();
                        return false;
                    }
                }
            } else if (window.localStorage.getItem('dpd_npp_margin_calculated') == 1) {
                $.cookie('dpd_payment_method', '');
                window.localStorage.setItem('dpd_npp_margin_calculated', 0);
                globalMessageList.addSuccessMessage({ 
                    message: 'Вы сменили способ оплаты. '+
                        'Подтвердите новую стоимость доставки. Идет переадресация...'

                });
                $('html, body').animate({
                    scrollTop: $('body').offset().top
                });

                $('[name="street[0]"]').keyup();
                
                setTimeout(function(){
                    stepNavigator.navigateTo('shipping');
                }, 3000);

                this.recalcShipping();
                return false;
            }
            $.cookie('dpd_payment_method', '');
            window.localStorage.getItem('dpd_npp_margin_calculated', 0);
            return true;
        },

        recalcShipping: function() {
            //get address from quote observable
            var address = quote.shippingAddress();

            //changes the object so observable sees it as changed
            //address.trigger_reload = new Date().getTime();

            //create rate registry cache
            //the two calls are required 
            //because Magento caches things
            //differently for new and existing
            //customers (a FFS moment)
            rateRegistry.set(address.getKey(), null);
            rateRegistry.set(address.getCacheKey(), null);

            //with rates cleared, the observable listeners should
            //update everything when the rates are updated
            quote.shippingAddress(address);
        },

        isChecked: ko.computed(function () {
            return quote.paymentMethod() ? quote.paymentMethod().method : null;
        }),

        isRadioButtonVisible: ko.computed(function () {
            return paymentService.getAvailablePaymentMethods().length !== 1;
        }),

        /**
         * Get payment method data
         */
        getData: function () {
            return {
                'method': this.item.method,
                'po_number': null,
                'additional_data': null
            };
        },

        /**
         * Get payment method type.
         */
        getTitle: function () {
            return this.item.title;
        },

        /**
         * Get payment method code.
         */
        getCode: function () {
            return this.item.method;
        },

        /**
         * @return {Boolean}
         */
        validate: function () {
            return true;
        },

        /**
         * @return {String}
         */
        getBillingAddressFormName: function () {
            return 'billing-address-form-' + this.item.method;
        },

        /**
         * Dispose billing address subscriptions
         */
        disposeSubscriptions: function () {
            // dispose all active subscriptions
            var billingAddressCode = 'billingAddress' + this.getCode();

            registry.async('checkoutProvider')(function (checkoutProvider) {
                checkoutProvider.off(billingAddressCode);
            });
        }
    });
});
