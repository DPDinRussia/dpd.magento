var config = {
    map: {
        '*': {
            'jquery.dpd.map': 'Pimentos_DPD/js/jquery.dpd.map',
            'Magento_Checkout/js/model/shipping-save-processor/default':
                'Pimentos_DPD/js/model/shipping-save-processor/default',
            'Magento_Checkout/js/model/checkout-data-resolver':
                'Pimentos_DPD/js/model/checkout-data-resolver',
            'Magento_Checkout/js/model/shipping-service':
                'Pimentos_DPD/js/model/shipping-service',
            'Magento_Checkout/js/view/shipping':
                'Pimentos_DPD/js/view/shipping',
            'Magento_Checkout/js/view/payment/default':
                'Pimentos_DPD/js/view/payment/default',
            'api-maps.yandex':
                'https://api-maps.yandex.ru/2.1/?lang=ru_RU',
        }
    }
};