require([
    'jquery',
    'jquery/ui'
    ], function($){
    $(document).ready(function() {

        checkEnableStatusFields();

        if ($('#sender_sender_group_sender_city_id').val()) {
          loadTerminalSelector(
            $('#sender_sender_address_to_terminal_group_sender_terminal_code'),
            $('#sender_sender_group_sender_city_id').val(),
            $('#sender_sender_address_to_terminal_group_sender_terminal_code_selected').val()
          );
        }

        $('#status_status_group_status_order_check').change(function(){
            checkEnableStatusFields();
        });

        if ($('#import_import_group').length) {
          $('#import_import_group').before(
            '<button id="dpd_run_import" title="Запустить импорт данных" type="button"' +
              'class="action-default scalable save primary' +
              'ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only">' +
              '<span class="ui-button-text">' +
              '<span>Запустить импорт данных</span>' +
              '</span>' +
            '</button>'
          );
          $(document.body).on('click', '#dpd_run_import', function(){
            var self = $(this);
            $.ajax({
              url: '../../../../../../../dpd/ajax/checkimportopportunity',
              type: 'post',
              dataType: 'json',
              data: {form_key: window.FORM_KEY},
              beforeSend: function(){
                self.attr('disabled', '');
              },
              success: function(response) {
                self.removeAttr('disabled');
                self.hide();
                if (response.check == 1) {
                  $('#messages').remove();
                  $('#import_import_group').before(
                    '<div id="progress_message" class="notice notice-info inline">'+
                        '<p>' +
                            'Шаг <span id="step"> 1 </span>' + 
                            ' из 4. ' +
                            '<span id="stepname">Импорт городов</span>' +
                        '</p>' +
                    '</div>' +
                    '<div class="process">' +
                        '<div class="progress-bar-wrapper">' +
                            '<div class="progress-bar"style="width: 0%">' +
                                '<span>0%</span>' +
                            '</div>' +
                        '</div>' +
                    '</div>'
                  );
                  runImportData(1, 0);
                } else {
                  if (!$('#messages').length) {
                    $('#dpd_run_import').before('<div id="messages">' +
                    '<div class="messages">' +
                      '<div class="message message-error error">' +
                        '<div data-ui-id="message-error error">'+
                          'Чтобы начать импорт, необходимо заполнить данные аккаунта' + 
                        '</div>' +
                      '</div>' +
                      '</div>' +
                    '</div>');
                  }
                }
              }
            });
          })
        }

        $(document.body).on('change','#sender_sender_address_to_terminal_group_sender_terminal_code',
          function(){
            var val = $(this).val();
            $('#sender_sender_address_to_terminal_group_sender_terminal_code_selected').val(val);
        })

        if ($('#sender_sender_group_sender_city').length) {
          $('#sender_sender_group_sender_city').autocomplete({
            source: function(request, response) {
              $.ajax( {
                url: '../../../../../../../dpd/ajax/citiesautocomplete',
                type: 'post',
                dataType: 'json',
                data: {
                  term: request.term,
                  form_key: window.FORM_KEY
                },
                success: function(data) {
                  response(data);
                }
              } );
            },
            minLength: 3,
            select: function(event, ui) {
              $('#sender_sender_group_sender_city_id').val(ui.item.id);
              loadTerminalSelector(
                $('#sender_sender_address_to_terminal_group_sender_terminal_code'),
                ui.item.id,
                0
              );
            }
          }).data('autocomplete')._renderItem = function(ul, item) {
              return $('<li></li>')
                .data('item.autocomplete', item)
                .append('<a>' + item.value+'</a>')
                .appendTo(ul);
          };
      }
    });

    function checkEnableStatusFields() {
        var val = $('#status_status_group_status_order_check').val();
        $('.dpd-status-sync').each(function(){
            if (val == '1') {
                $(this).removeAttr('disabled');
            } else {
                $(this).attr('disabled', '');
            }
        });
    }

    function loadTerminalSelector(selector, cityId, value, disabled) {
      $.ajax({
        url: '../../../../../../../dpd/ajax/terminalsbycityid',
        type: 'post',
        dataType: 'json',
        data: {
          city_id: cityId,
          form_key: window.FORM_KEY
        },
        beforeSend: function() {
          selector.html('').attr('disabled', '');
        },
        success: function(response) {
          selector.html(response.html)
            .removeAttr('disabled');
          if (value) {
            selector.val(value);
          }
          if (disabled) {
            selector.attr('disabled', '');
          }
        },
        error: function() {
          alert('Connection problem. Please, try later.');
        }
      });
    }

    function runImportData(step, offset) {
        $.ajax({
            url: '../../../../../../../dpd/ajax/importdata',
            type: 'post',
            dataType: 'json',
            data: {
              step: step,
              offset: offset,
              form_key: window.FORM_KEY
            },
            success: function(response) {
                if (response.error) {
                    $('#progress_message').removeClass('notice-info');
                    $('#progress_message').addClass('notice-error');
                    $('#progress_message p').text(response.error);
                } else {
                    $('.progress-bar').attr('style', 'width:' + response.percent + '%');
                    $('.progress-bar span').text(response.percent + '%');
                    $('#progress_message p #step').text(Number(response.step) + 1);
                    $('#progress_message p #stepname').text(response.stepname);
                    if (response.step == -1){
                        $('.progress-bar-wrapper').attr('style', 'display:none');
                        $('#progress_message').removeClass('notice-info');
                        $('#progress_message').addClass('notice-success');
                        $('#progress_message p').text(response.stepname);
                        setTimeout(function(){
                            location.href = location.href + '&action=importDone';
                        }, 2000);
                    } else {
                        runImportData(response.step, response.offset);
                    }
                }
            },
            error: function() {
                alert('Connection problem. Let\'s try again.');
                runImportData(step, offset);
            }
        });
    }
});