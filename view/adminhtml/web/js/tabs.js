require([
    'jquery',
    'jquery/ui'
    ], function($){
    $(document).ready(function() {
        $(document).on('click', '.dpd-tab', function(e){
            e.preventDefault();
            var self = $(this);
            var tabsContentLevel = self.closest('nav').data('tabs-content-level');
            self.parent().find('.dpd-tab').removeClass('nav-tab-active');
            self.addClass('nav-tab-active');
            $('.dpd-tab-content-' + tabsContentLevel).hide();
            $('#' + self.data('tab-content-id')).show();
        });
    });
});