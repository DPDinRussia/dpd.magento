require([
    'jquery'
    ], function($){
        jQuery(document).on('change','#dpd_order .need-reload-after-change', function(e){
            var self = jQuery(this);
            var form = self.closest('form');
            var formData = form.serialize();
            jQuery('#dpd_order .send-order').attr('disabled', '');
            proccessOrderForm(
                jQuery('#dpd_order').data('change-url') + 
                    '?dpdActiveTab=' + 
                    jQuery('#dpd_order .nav-tab.dpd-tab.nav-tab-active').data('tab-content-id'),
                formData,
                'post',
                null,
                function(html){
                    jQuery('#dpd_order .send-order').removeAttr('disabled');
                    jQuery(document).find('#dpd_order').html(
                       jQuery(html).find('#dpd_order').html() 
                    );
            });
        });

        jQuery(document).on('click', '#dpd_order .send-order', function(e){
            e.preventDefault();
            var self = jQuery(this);
            var form = self.closest('form');
            var formData = form.serialize();
            self.attr('disabled', '');
            proccessOrderForm(jQuery('#dpd_order').data('send-url'),
                formData, 'post', 'json', function(response){
                self.removeAttr('disabled');
                
                if (response.data) {
                    self.hide();
                    
                    jQuery('#dpd_order .cancel-order').show();
                    jQuery('#dpd_id').text(response.data.id);
                    jQuery('#dpd_status').text(response.data.status);

                    if (response.data.id) {
                        jQuery('#dpd_order_form_files').show();
                        jQuery('#dpd_order_form_files-message').hide();

                        form.find('input,select', '#dpd_order_form_files').each(function(){
                            jQuery(this).attr('disabled', '');
                        });
                    }
                }
            });
        });

        jQuery(document).on('click', '#dpd_order .cancel-order', function(e){
            e.preventDefault();
            var form = jQuery('#dpd_order');
            jQuery(this).attr('disabled', '');
            proccessOrderForm(jQuery('#dpd_order').data('cancel-url') + 
                '?order_id=' + jQuery('#dpd_order input[name="order[orderId]"]').val(),
                'empty',
                'post',
                'json',
                function(response){
                    jQuery('#dpd_order .cancel-order').removeAttr('disabled');
                    if (!response.error) {
                        jQuery('#dpd_order .cancel-order').hide();
                        jQuery('#dpd_order_form_files').hide();
                        jQuery('#dpd_order .send-order').show();
                        jQuery('#dpd_id').text('');
                        jQuery('#dpd_status').text(response.data.status);
                        form.find('input,select').each(function(){
                            jQuery(this).removeAttr('disabled');
                        });

                        jQuery('#dpd_order_form_files-message').show();
                        jQuery('#dpd_order_form_files').hide();

                    } else {
                        form.find('input,select').each(function(){
                            jQuery(this).attr('disabled', '');
                        });
                    }
            });
        });

        jQuery(document).on('click', '#dpd_order #download_invoice_file', function(e){
            e.preventDefault();
            var self = jQuery(this);
            printDocs(jQuery(this).data('invoice-file-url') + 
                '&order_id=' + jQuery('#dpd_order input[name="order[orderId]"]').val(), 
                self
            );            
        });

        jQuery(document).on('click', '#dpd_order #download_label_file', function(e){
            e.preventDefault();
            var self = jQuery(this);
            printDocs(jQuery(this).data('label-file-url') + 
                    '&order_id=' + jQuery('#dpd_order input[name="order[orderId]"]').val() +
                    '&label_count=' + jQuery('#dpd_order #dpd_label_count').val() +
                    '&file_format=' + jQuery('#dpd_order #dpd_file_format').val() +
                    '&print_area_format=' + jQuery('#dpd_order #dpd_print_area_format').val(),
                self
            );            
        });

        function printDocs(url, button) {
            button.attr('disabled', '');
            $.ajax({
                url: url,
                type: 'post',
                data: 'form_key=' + window.FORM_KEY + '&isAjax=true',
                dataType: 'json',
                success: function(response) {
                    button.removeAttr('disabled');
                    if (response.error) {
                        jQuery('.messages').first().html(
                            '<div class="message message-error error">'+
                            '<div data-ui-id="messages-message-error">' + 
                            response.error + '</div></div>'
                        );  
                    } else {
                        if (button.next('a').length > 0) {
                            button.next('a').attr('href', response.file);
                        } else {
                            jQuery('<a />')
                                .attr('href', response.file)
                                .attr('target', '_blank')
                                .attr('style', 'display: inline-block; margin-left: 20px')
                                .html('скачать файл')
                                .insertAfter(button)
                            ;
                        }
                    }
                }
            });
        }

        function proccessOrderForm(url, data, type, dataType, callback) {
            var orderForm = jQuery('#dpd_order');
            $.ajax({
                url: url,
                data: data + '&form_key=' + window.FORM_KEY + '&isAjax=true',
                type: type,
                dataType: dataType,
                beforeSend: function(){
                    jQuery('.notifications').html('');
                    orderForm.find('input,select').each(function(){
                        jQuery(this).attr('disabled', '');
                    });
                },
                success: function(response){
                    jQuery('html, body').animate({ scrollTop: 0 }, 'slow');
                    if (response.error) {
                        orderForm.find('input,select').each(function(){
                            jQuery(this).removeAttr('disabled');
                        });
                        jQuery('.messages').first().html(
                            '<div class="message message-error error">'+
                            '<div data-ui-id="messages-message-error">' + 
                            response.error + '</div></div>'
                        );                        
                    } else {
                        jQuery('.messages').first().html(
                            '<div class="message message-success success">'+
                            '<div data-ui-id="messages-message-success">' + 
                            response.success + '</div></div>'
                        );
                    }
                    callback(response);
                }
            });
        }
});