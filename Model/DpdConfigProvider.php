<?php

namespace Pimentos\DPD\Model;

use Magento\Checkout\Model\ConfigProviderInterface;

class DpdConfigProvider implements ConfigProviderInterface
{

    /**
     * @var \Ipol\DPD\Config\Config
     */
    protected $configArray;

    /**
     * @var \Magento\Tax\Api\TaxCalculationInterface
     */
    protected $helperData;

    /**
     * @param Action\Context 
     */
    public function __construct(
        \Pimentos\DPD\Helper\Data $helperData
    ) {
        $this->helperData = $helperData;
        $this->configArray = $helperData->getDpdSdkConfigArray();
    }

    /**
     * {@inheritdoc}
     */
    public function getConfig()
    {
        unset($this->configArray['KLIENT_KEY']);
        unset($this->configArray['KLIENT_KEY_BY']);
        unset($this->configArray['KLIENT_KEY_KZ']);
        unset($this->configArray['KLIENT_NUMBER']);
        unset($this->configArray['KLIENT_NUMBER_BY']);
        unset($this->configArray['KLIENT_NUMBER_KZ']);
        return $this->configArray;
    }
}