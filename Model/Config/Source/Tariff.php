<?php

namespace Pimentos\DPD\Model\Config\Source;

class Tariff implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * {@inheritdoc}
     *
     * @codeCoverageIgnore
     */
    public function toOptionArray()
    {
        return [
            ['value' => '', 'label' => '----'],
            ['value' => 'PCL', 'label' => 'DPD OPTIMUM'],
            ['value' => 'CUR', 'label' => 'DPD CLASSIC'],
            ['value' => 'CSM', 'label' => 'DPD Online Express'],
            ['value' => 'ECN', 'label' => 'DPD ECONOMY'],
            ['value' => 'ECU', 'label' => 'DPD ECONOMY CU'],
            ['value' => 'NDY', 'label' => 'DPD EXPRESS'],
            ['value' => 'BZP', 'label' => 'DPD 18:00'],
            ['value' => 'MXO', 'label' => 'DPD Online Max'],
            ['value' => 'MAX', 'label' => 'DPD MAX domestic'],
            ['value' => 'PUP', 'label' => 'DPD SHOP'],
        ];
    }
}
