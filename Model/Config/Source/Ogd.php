<?php

namespace Pimentos\DPD\Model\Config\Source;

class Ogd implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * {@inheritdoc}
     *
     * @codeCoverageIgnore
     */
    public function toOptionArray()
    {
        return [
            ['value' => '', 'label' => '- Не установлено -'],
            ['value' => 'ПРИМ', 'label' => 'Примерка'],
            ['value' => 'ПРОС', 'label' => 'Простая'],
            ['value' => 'РАБТ', 'label' => 'Проверка работоспособности'],
        ];
    }
}
