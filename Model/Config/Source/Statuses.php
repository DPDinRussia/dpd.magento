<?php

namespace Pimentos\DPD\Model\Config\Source;

class Statuses implements \Magento\Framework\Option\ArrayInterface
{
    protected $statusCollection;

    public function __construct(
        \Magento\Sales\Model\ResourceModel\Order\Status\Collection $statusCollection
    ) {
        $this->statusCollection = $statusCollection;
    }
    /**
     * {@inheritdoc}
     *
     * @codeCoverageIgnore
     */
    public function toOptionArray()
    {
        $collection =  $this->statusCollection->toOptionArray();
        return array_merge([['value' => '', 'label' => '- Не задано -']], $collection);

    }
}
