<?php

namespace Pimentos\DPD\Model\Config\Source;

class Accounts implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * {@inheritdoc}
     *
     * @codeCoverageIgnore
     */
    public function toOptionArray()
    {
        return [
            ['value' => 'RU', 'label' => 'Россия'],
            ['value' => 'KZ', 'label' => 'Казахстан'],
            ['value' => 'BY', 'label' => 'Беларусь'],
        ];
    }
}
