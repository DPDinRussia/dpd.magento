<?php

namespace Pimentos\DPD\Model\Config\Source;

class ShopPaymentTypes implements \Magento\Framework\Option\ArrayInterface
{
    public $scope;

    public function __construct(\Magento\Framework\App\Config\ScopeConfigInterface $scope)
    {
        $this->scope = $scope;
    }

    /**
     * {@inheritdoc}
     *
     * @codeCoverageIgnore
     */
    public function toOptionArray()
    {
        $methodList = $this->scope->getValue('payment');
        $payments = [];
        $payments[] = [
            'value' => '',
            'label' => '----'
        ];
        foreach ($methodList as $code => $method) {
            if (isset($method['active'], $method['title']) && $method['active']) {
                $payments[] = [
                    'value' => $code,
                    'label' => $method['title']
                ];
            }
        }
        return $payments;
    }
}
