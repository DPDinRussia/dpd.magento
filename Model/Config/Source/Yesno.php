<?php

namespace Pimentos\DPD\Model\Config\Source;

class Yesno implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * {@inheritdoc}
     *
     * @codeCoverageIgnore
     */
    public function toOptionArray()
    {
        return [
            ['value' => '0', 'label' => 'Нет'],
            ['value' => '1', 'label' => 'Да']
        ];
    }
}
