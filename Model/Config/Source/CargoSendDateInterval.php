<?php

namespace Pimentos\DPD\Model\Config\Source;

class CargoSendDateInterval implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * {@inheritdoc}
     *
     * @codeCoverageIgnore
     */
    public function toOptionArray()
    {
        return [
            ['value' => '9-18', 'label' => 'в любое время с 09:00 до 18:00'],
            ['value' => '9-13', 'label' => 'с 09:00 до 13:00'],
            ['value' => '13-18', 'label' => 'с 13:00 до 18:00'],
            ['value' => '18-22', 'label' => 'с 18:00 да 22:00 (оплачивается дополнительно)'],
        ];
    }
}
