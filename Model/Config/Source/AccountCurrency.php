<?php

namespace Pimentos\DPD\Model\Config\Source;

class AccountCurrency implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * {@inheritdoc}
     *
     * @codeCoverageIgnore
     */
    public function toOptionArray()
    {
        return [
            ['value' => 'KZT', 'label' => 'Тенге'],
            ['value' => 'RUB', 'label' => 'Рубль'],
            ['value' => 'USD', 'label' => 'Доллар'],
            ['value' => 'EUR', 'label' => 'Евро'],
            ['value' => 'UAH', 'label' => 'Гривна'],
            ['value' => 'BYR', 'label' => 'Белорусский рубль']
        ];
    }
}
