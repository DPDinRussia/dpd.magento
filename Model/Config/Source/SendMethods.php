<?php

namespace Pimentos\DPD\Model\Config\Source;

class SendMethods implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * {@inheritdoc}
     *
     * @codeCoverageIgnore
     */
    public function toOptionArray()
    {
        return [
            ['value' => '1', 'label' => 'Мы будем возить заказы сами'],
            ['value' => '0', 'label' => 'Мы хотим вызывать забор автоматически']
        ];
    }
}
