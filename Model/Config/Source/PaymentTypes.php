<?php

namespace Pimentos\DPD\Model\Config\Source;

class PaymentTypes implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * {@inheritdoc}
     *
     * @codeCoverageIgnore
     */
    public function toOptionArray()
    {
        return [
            ['value' => '', 'label' => 'У отправителя по безналичному расчёту'],
            ['value' => 'ОУП', 'label' => 'Оплата у получателя наличными'],
            ['value' => 'ОУО', 'label' => 'Оплата у отправителя наличными'],
        ];
    }
}
