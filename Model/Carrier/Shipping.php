<?php
namespace Pimentos\DPD\Model\Carrier;

use Magento\Quote\Model\Quote\Address\RateRequest;
use Magento\Shipping\Model\Rate\Result;
use Magento\Store\Model\ScopeInterface;

use Pimentos\DPD\Helper\PriceRules;

class Shipping extends \Magento\Shipping\Model\Carrier\AbstractCarrier implements
    \Magento\Shipping\Model\Carrier\CarrierInterface
{
    /**
     * @var string
     */
    protected $_code = 'dpd';

    /**
     * @var string
     */
    protected $courierCode = 'courier';

    /**
     * @var string
     */
    protected $pickupCode = 'pickup';

    /**
     * @var string
     */
    protected $name = 'DPD';

    /**
     * @var \Magento\Shipping\Model\Rate\ResultFactory
     */
    protected $_rateResultFactory;

    /**
     * @var \Magento\Quote\Model\Quote\Address\RateResult\MethodFactory
     */
    protected $_rateMethodFactory;

    /**
     * @var \Pimentos\DPD\Helper\Data
     */
    protected $config;

    /**
     * @var \Pimentos\DPD\Helper\Data
     */
    protected $scopeConfig;

    /**
     * @var \Pimentos\DPD\Helper\Data
     */
    protected $helperData;

    /**
     * @var \Magento\Tax\Api\TaxCalculationInterface
     */
    protected $taxCalculation;

    /**
     * @var \Magento\Framework\App\ObjectManager
     */
    protected $objectManager;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * Shipping constructor.
     *
     * @param \Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory  $rateErrorFactory
     * @param \Psr\Log\LoggerInterface                                    $logger
     * @param \Magento\Shipping\Model\Rate\ResultFactory                  $rateResultFactory
     * @param \Magento\Quote\Model\Quote\Address\RateResult\MethodFactory $rateMethodFactory
     * @param array                                                       $data
     */
    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory $rateErrorFactory,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Shipping\Model\Rate\ResultFactory $rateResultFactory,
        \Magento\Quote\Model\Quote\Address\RateResult\MethodFactory $rateMethodFactory,
        \Magento\Tax\Api\TaxCalculationInterface $taxCalculation,
        \Pimentos\DPD\Helper\Data $helperData,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        array $data = []
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->helperData = $helperData;
        $this->config = $helperData->generateDpdSdkConfig();
        $this->_rateResultFactory = $rateResultFactory;
        $this->_rateMethodFactory = $rateMethodFactory;
        $this->taxCalculation = $taxCalculation;
        $this->storeManager = $storeManager;
        $this->objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        parent::__construct($scopeConfig, $rateErrorFactory, $logger, $data);
    }

    /**
     * get allowed methods
     * @return array
     */
    public function getAllowedMethods()
    {
        return [$this->_code => $this->name];
    }

    /**
     * @param RateRequest $request
     * @return bool|Result
     */
    public function collectRates(RateRequest $request)
    {
        if (!$this->getConfigFlag('active') ||
            ((!$this->scopeConfig
            ->getValue('common/dpd_ru/client_number_RU', ScopeInterface::SCOPE_STORE) &&
            !$this->scopeConfig->getValue('common/dpd_ru/auth_key_RU', ScopeInterface::SCOPE_STORE)) &&
            (!$this->scopeConfig
                ->getValue('common/dpd_kz/client_number_KZ', ScopeInterface::SCOPE_STORE) &&
            !$this->scopeConfig->getValue('common/dpd_kz/auth_key_KZ', ScopeInterface::SCOPE_STORE) &&
            (!$this->scopeConfig
                ->getValue('common/dpd_by/client_number_BY', ScopeInterface::SCOPE_STORE) &&
            !$this->scopeConfig->getValue('common/dpd_by/auth_key_BY', ScopeInterface::SCOPE_STORE))))
        ) {
            return false;
        }

        $paymentMethod = $_COOKIE['dpd_payment_method'] ?? '';

        //создаем конвертер
        $converter = new \Pimentos\DPD\Helper\Converter();

        //создаем отправку
        $shipmentFactory = new \Pimentos\DPD\Builder\Shipment(
            $this->config,
            $this->helperData->getData(
                'sender/sender_group/sender_city'
            ),
            $request->getDestCountryId(),
            $request->getDestRegionCode(),
            $request->getDestCity(),
            $request->getOrderSubtotal(),
            $request->getAllItems(),
            $this->taxCalculation
        );
        $shipment = $shipmentFactory->getInstance();

        if ($paymentMethod) {
            $shipment->setPaymentMethod(1, $paymentMethod);
        }


        /** @var \Magento\Shipping\Model\Rate\Result $result */
        $result = $this->_rateResultFactory->create();

        try {
            //получаем способ доставки курьером
            $shipment->setSelfDelivery(false);
            $shipment->setSelfPickup(
                $this->helperData->getData(
                    'shipping_description/shipping_description_group/self_pickup'
                )
            );

            $calc = $shipment->calculator();
            $calc->setCurrencyConverter($converter);
            $tariff = $calc->calculate(
                $this->storeManager->getStore()->getBaseCurrencyCode()
            );
            if ($shipment->isPossibileDelivery()) {
                $tariff = PriceRules::round(
                    $tariff,
                    $this->helperData->getData(
                        'calculation/calculation_group/round_to'
                    )
                );

                /** @var \Magento\Quote\Model\Quote\Address\RateResult\Method $method */
                $method = $this->_rateMethodFactory->create();
                $method->setCarrier($this->_code);
                $method->setCarrierTitle($this->getConfigData('courier_title'));
                $method->setMethod($this->courierCode);
                $method->setMethodTitle($this->name);
                
                $amount = $tariff['COST'];
                
                $method->setPrice($amount);
                $method->setCost($amount);
                $result->append($method);
            }

            
            //получаем способ доставки самовывоз
            $shipment->setSelfDelivery(true);
            $tariff = $calc->calculate(
                $this->storeManager->getStore()->getBaseCurrencyCode()
            );
            if ($shipment->isPossibileSelfDelivery()) {
                $tariff = PriceRules::round(
                    $tariff,
                    $this->helperData->getData(
                        'calculation/calculation_group/round_to'
                    )
                );

                /** @var \Magento\Quote\Model\Quote\Address\RateResult\Method $method */
                $method = $this->_rateMethodFactory->create();
                $method->setCarrier($this->_code);
                $method->setCarrierTitle($this->getConfigData('pickpoint_title'));
                $method->setMethod($this->pickupCode);
                $method->setMethodTitle($this->name);
                
                $amount = $tariff['COST'];
                
                $method->setPrice($amount);
                $method->setCost($amount);
                $result->append($method);
            }
            
        } catch(\SoapFault $e) {
            $error = $this->_rateErrorFactory->create();
            $error->setCarrier($this->_code);
            $error->setCarrierTitle($this->_code);
            $error->setErrorMessage($e->getMessage());
            return $error;
        }
        catch(\Exception $e) {
            $error = $this->_rateErrorFactory->create();
            $error->setCarrier($this->_code);
            $error->setCarrierTitle($this->_code);
            $error->setErrorMessage($e->getMessage());
            return $error;
        }

        return $result;
    }
}