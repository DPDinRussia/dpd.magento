<?php

namespace Pimentos\DPD\Model\SDK;

class Location extends Model {

    public function __construct($DPDconfig) {
        $db = \Ipol\DPD\DB\Connection::getInstance($DPDconfig);
        $this->table = $db->getTable('location');
    }

    public static function formAutoCompleteArray(array $locations, string $countryCode = null)
    {
        $result = [];
        foreach ($locations as $item) {
            $name = explode(',', $item['ORIG_NAME']);
            $result[] = [
                $countryCode ? 
                    trim($name[2]).', '.trim($name[1]) : $item['ORIG_NAME'],
                $item['CITY_ID']
            ];
        }
        return $result;
    }
}